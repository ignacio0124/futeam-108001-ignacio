<?php

/**
 * @since 03/21/19 重甫: 增加登入判斷
 */

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
use App\Service\LoginService;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct() {
    //     $this->middleware('guest')->except('logout');
    // }

    /**
     * 注入的參數
     *
     * @var UserAccountService
     */
    protected $LoginService;

    /**
     * LoginController constructor
     *
     * @param LoginService $loginService
     */
    public function __construct(LoginService $loginService) {
        $this->LoginService = $loginService;
    }

    /**
     * 登入驗證
     *
     * @param Request $request
     */
    public function Login(Request $request) {
        /** @var string 帳號 */
        $account = $request->input('account');
        /** @var string 密碼 */
        $password = $request->input('password');

        $result = $this->LoginService->isUser($account, $password);

        return json_encode($result);
    }

    /**
     * 登出
     */
    public function Logout() {
        Auth::logout();
        return redirect('/');
    }
}
