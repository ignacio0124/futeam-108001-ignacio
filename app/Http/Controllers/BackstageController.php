<?php
/**
 * 後台管理的Controller
 *
 * @author 重甫
 * @since 05/20/19
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\MemberRepository;

/**
 * Class BackstageController
 *
 * @package App\Http\Controllers
 */
class BackstageController extends Controller
{
    /**
     * 注入的參數
     *
     * @var MemberRepository
     */
    protected $MemberRepository;

    /**
     * BackstageController constructor
     *
     * @param MemberRepository $memberRepository
     */
    public function __construct(MemberRepository $memberRepository) {
        /** 權限判斷 */
        $this->middleware(function ($request, $next) {
            if (!Auth::check()) {
                return redirect('/')->send();
            } else {
                return $next($request);
            }
        });

        $this->MemberRepository = $memberRepository;
    }

    /**
     * 顯示後台成員管理頁面
     *
     * @return \Illuminate\View\View 後台成員管理頁面
     */
    public function viewBackstageMemberPage() {
        /** @var string 帳號 */
        $account = Auth::user()->Account;
        /** @var \Illuminate\Database\Eloquent\Collection 成員 */
        $member = $this->MemberRepository->getMember();

        return view('Backstage_Member', compact('account', 'member'));
    }

    /**
     * 新增成員
     *
     * @param  Request $request
     * @return int MemberId
     */
    public function newMember(Request $request) {
        /** @var string 人名 */
        $name = $request->input('name');
        /** @var string 帳號 */
        $account = is_null($request->input('account')) ? "" : $request->input('account');

        return $this->MemberRepository->newMember($name, $account);
    }

    /**
     * 刪除成員
     *
     * @param  Request $request
     */
    public function deleteMember(Request $request) {
        /** @var int MemberId */
        $member_id = $request->input('member_id');

        $this->MemberRepository->deleteMember($member_id);
    }

    /**
     * 設定成員
     *
     * @param Request $request
     */
    public function setMember(Request $request) {
        /** @var int MemberId */
        $member_id = $request->input('member_id');
        /** @var string 人名 */
        $name = $request->input('name');
        /** @var string 帳號 */
        $account = $request->input('account');

        $this->MemberRepository->setMember($member_id, $name, $account);
    }
}
