<?php
/**
 * 主頁的Controller
 *
 * @author 重甫
 * @since 03/27/19
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\CrawlerService;
use App\Service\MainService;
use App\Repository\WebsiteRepository;
use App\Repository\MemberRepository;
use App\Repository\UserWebsiteRepository;
use App\Repository\UserWebsiteContentRepository;
use App\Repository\UserWebsiteContentMemberRepository;
use App\Repository\ReportRepository;
use App\Repository\ReportMemberRepository;

/**
 * Class MainController
 *
 * @package App\Http\Controllers
 */
class MainController extends Controller
{
    /**
     * 注入的參數
     *
     * @var CrawlerService
     * @var MainService
     * @var WebsiteRepository
     * @var MemberRepository
     * @var UserWebsiteRepository
     * @var UserWebsiteContentRepository
     * @var UserWebsiteContentMemberRepository
     * @var ReportRepository
     * @var ReportMemberRepository
     */
    protected $CrawlerService;
    protected $MainService;
    protected $WebsiteRepository;
    protected $MemberRepository;
    protected $UserWebsiteRepository;
    protected $UserWebsiteContentRepository;
    protected $UserWebsiteContentMemberRepository;
    protected $ReportRepository;
    protected $ReportMemberRepository;

    /**
     * MainController constructor
     *
     * @param CrawlerService $crawlerService
     * @param MainService $mainService
     * @param WebsiteRepository $websiteRepository
     * @param MemberRepository $memberRepository
     * @param UserWebsiteRepository $userWebsiteRepository
     * @param UserWebsiteContentRepository $userWebsiteContentRepository
     * @param UserWebsiteContentMemberRepository $userWebsiteContentMemberRepository
     * @param ReportRepository $reportRepository
     * @param ReportMemberRepository $reportMemberRepository
     */
    public function __construct(CrawlerService $crawlerService,
                                MainService $mainService,
                                WebsiteRepository $websiteRepository,
                                MemberRepository $memberRepository,
                                UserWebsiteRepository $userWebsiteRepository,
                                UserWebsiteContentRepository $userWebsiteContentRepository,
                                UserWebsiteContentMemberRepository $userWebsiteContentMemberRepository,
                                ReportRepository $reportRepository,
                                ReportMemberRepository $reportMemberRepository) {
        /** 權限判斷 */
        $this->middleware(function ($request, $next) {
            if (!Auth::check()) {
                return redirect('/')->send();
            } else {
                return $next($request);
            }
        });

        $this->CrawlerService = $crawlerService;
        $this->MainService = $mainService;
        $this->WebsiteRepository = $websiteRepository;
        $this->MemberRepository = $memberRepository;
        $this->UserWebsiteRepository = $userWebsiteRepository;
        $this->UserWebsiteContentRepository = $userWebsiteContentRepository;
        $this->UserWebsiteContentMemberRepository = $userWebsiteContentMemberRepository;
        $this->ReportRepository = $reportRepository;
        $this->ReportMemberRepository = $reportMemberRepository;
    }

    /**
     * 顯示主頁
     *
     * @return \Illuminate\View\View 主頁
     */
    public function viewMainPage() {
        /** @var \Illuminate\Database\Eloquent\Collection 成員 */
        $member = $this->MemberRepository->getMember();
        /** @var \Illuminate\Database\Eloquent\Collection 全部的網站 */
        $website = $this->WebsiteRepository->getAllWebsite();
        /** @var \Illuminate\Database\Eloquent\Collection 使用者擁有的網站資訊 */
        $user_website = $this->UserWebsiteRepository->getUserWebsite();
        /** @var int 總共有幾筆網站資訊 */
        $total_user_website = count($user_website);
        /** @var string 今天的日期 */
        $date = date('Y-m-d');
        /** @var string 帳號 */
        $account = Auth::user()->Account;

        return view('Main', compact('date', 'website', 'user_website', 'total_user_website', 'account', 'member'));
    }

    /**
     * 顯示計算彈跳視窗
     *
     * @param  Request $request
     * @return \Illuminate\View\View 彈跳視窗頁面
     */
    public function viewPopupCalculate(Request $request) {
        /** @var int 第幾列 */
        $row = $request->input('row');
        /** @var int 第幾群組 */
        $group = $request->input('group');
        /** @var array 下拉欄位 */
        $crawler_column = $request->input('crawler_column');
        /** @var array 計算欄位值 */
        $calculate = $request->input('calculate');

        return view('Popup_Calculate', compact('crawler_column', 'row', 'group', 'calculate'));
    }

    /**
     * 取得爬蟲抓取的欄位值
     *
     * @param  Request $request
     * @return array 爬蟲抓取的欄位值
     */
    public function getCrawlerColumn(Request $request) {
        /** @var string 網址 */
        $url = $request->input('url');
        /** @var mixed 帳號 */
        $account = $request->input('account');
        /** @var mixed 密碼 */
        $password = $request->input('password');
        /** @var string 起始日期 */
        $date1 = $request->input('date1');
        /** @var string 結束日期 */
        $date2 = $request->input('date2');

        return $this->CrawlerService->callCrawler($url, $account, $password, $date1, $date2);
    }

    /**
     * 取得增加欄位要插入的div
     *
     * @param  Request $request
     * @return \Illuminate\View\View 增加欄位的div
     */
    public function getRowDiv(Request $request) {
        /** @var int 第幾列 */
        $row = $request->input('row');
        /** @var \Illuminate\Database\Eloquent\Collection 成員 */
        $member = $this->MemberRepository->getMember();
        /** @var string 今天的日期 */
        $date = date('Y-m-d');
        /** @var \Illuminate\Database\Eloquent\Collection 全部的網站 */
        $website = $this->WebsiteRepository->getAllWebsite();
        /** @var string 帳號 */
        $account = Auth::user()->Account;

        return view('Main_RowDiv', compact('row', 'date', 'website', 'account', 'member'));
    }

    /**
     * 取得增加組別要插入的div
     *
     * @param  Request $request
     * @return \Illuminate\View\View 增加組別的div
     */
    public function getGroupDiv(Request $request) {
        /** @var int 第幾列 */
        $row = $request->input('row');
        /** @var int 流水號 */
        $serial_number = $request->input('count_group');
        /** @var int 第幾群組 */
        $group = $request->input('last_group') + 1;
        /** @var \Illuminate\Database\Eloquent\Collection 成員 */
        $member = $this->MemberRepository->getMember();

        return view('Main_GroupDiv', compact('row', 'serial_number', 'group', 'member'));
    }

    /**
     * 設定使用者的網站
     *
     * @param Request $request
     * @return int 使用者網站的ID
     */
    public function setUserWebsite(Request $request) {
        /** @var int WebsiteId */
        $website_id = $request->input('website_id');
        /** @var string 帳號 */
        $account = $request->input('account');
        /** @var string 密碼 */
        $password = $request->input('password');
        /** @var int 使用者網站的ID */
        $user_website = $this->UserWebsiteRepository->setUserWebsite($website_id, $account, $password);

        return $user_website->UserWebsiteId;
    }

    /**
     * 設定儲存內容
     *
     * @param Request $request
     * @return int 網站內容ID
     */
    public function setSaveContent(Request $request) {
        /** @var array 要儲存的內容 */
        $website_content = $request->input('website_content');

        return $this->MainService->setSaveContent($website_content);
    }

    /**
     * 刪除儲存網站內容
     *
     * @param Request $request
     */
    public function deleteWebsiteContent(Request $request) {
        /** @var int 使用者儲存網站內容的id */
        $user_website_content_id = $request->input('user_website_content_id');

        $result_content = $this->UserWebsiteContentRepository->deleteWebsiteContent($user_website_content_id);
        $result_member = $this->UserWebsiteContentMemberRepository->deleteMemberContent($user_website_content_id);
        $report_member_id = $this->ReportRepository->getReportMemberIdFromUserWebsiteContentId($user_website_content_id);
        $this->ReportMemberRepository->deleteMember($report_member_id);
        $this->ReportRepository->deleteOne($user_website_content_id);
    }

}
