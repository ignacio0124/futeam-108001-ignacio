<?php
/**
 * 報表的Controller
 *
 * @author 重甫
 * @since 05/05/19
 */
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\ReportService;
use App\Repository\ReportRepository;
use App\Repository\MemberRepository;
use App\Repository\UserWebsiteRepository;

/**
 * Class ReportController
 *
 * @package App\Http\Controllers
 */
class ReportController extends Controller
{
    /**
     * 注入的參數
     *
     * @var ReportService
     * @var ReportRepository
     * @var MemberRepository
     * @var UserWebsiteRepository
     */
    protected $ReportService;
    protected $ReportRepository;
    protected $MemberRepository;
    protected $UserWebsiteRepository;

    /**
     * ReportController constructor
     *
     * @param ReportService $reportService
     * @param ReportRepository $reportRepository
     * @param MemberRepository $memberRepository
     * @param UserWebsiteRepository $userWebsiteRepository
     */
    public function __construct(ReportService $reportService,
                                ReportRepository $reportRepository,
                                MemberRepository $memberRepository,
                                UserWebsiteRepository $userWebsiteRepository) {
        /** 權限判斷 */
        $this->middleware(function ($request, $next) {
            if (!Auth::check()) {
                return redirect('/')->send();
            } else {
                return $next($request);
            }
        });

        $this->ReportService = $reportService;
        $this->ReportRepository = $reportRepository;
        $this->MemberRepository = $memberRepository;
        $this->UserWebsiteRepository = $userWebsiteRepository;
    }

    /**
     * 顯示報表頁
     *
     * @param  string $date GET的值
     * @return \Illuminate\View\View 主頁
     */
    public function viewReportPage($date = "") {
        /** @var string 日期 */
        $date = $this->ReportService->isValue("date", $date);
         /** @var \Illuminate\Database\Eloquent\Collection 把相同的日期群組起來 */
        $report_date_group = $this->ReportService->getWebsite("group_date", $date);
        /** @var \Illuminate\Database\Eloquent\Collection 有報表的網站 */
        $report_website = $this->ReportService->getWebsite("", $date);
        /** @var \Illuminate\Database\Eloquent\Collection 取得自己的成員 */
        $member = $this->MemberRepository->getMember();
        /** @var \Illuminate\Database\Eloquent\Collection 使用者擁有的網站 */
        $user_website = $this->UserWebsiteRepository->getUserWebsite();
        /** @var string 帳號 */
        $account = Auth::user()->Account;

        return view('Report', compact('report_website', 'report_website_group', 'report_date_group', 'member', 'account', 'date', 'user_website'));
    }

    /**
     * 設定報表
     *
     * @param  Request $request JS POST的值
     * @return object 設定報表後的結果
     */
    public function setReport(Request $request) {
        /** @var array 報表內容 */
        $report_content = $request->input("report");
        /** @var int 網站內容ID */
        $user_website_content_id = $request->input("user_website_content_id");
        /** @var boolean 新增報表後的正確錯誤回傳 */
        $result = $this->ReportService->setReport($report_content, $user_website_content_id);

        return response()->json($result);
    }
}
