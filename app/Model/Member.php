<?php
/**
 * Member Model
 *
 * @author 重甫
 * @since 04/23/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Member
 *
 * @package App\Member
 */
class Member extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Member';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'MemberId';

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;
}
