<?php
/**
 * Report Model
 *
 * @author 重甫
 * @since 05/06/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Report
 *
 * @package App\Report
 */
class Report extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Report';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ReportId';

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * 設定與ReportMember關聯
     */
    public function ReportMember() {
        return $this->belongsTo(
            'App\Model\ReportMember', 'ReportMemberId', 'ReportMemberId'
        );
    }

    /**
     * 設定與Website關聯
     */
    public function Website() {
        return $this->belongsTo(
            'App\Model\Website', 'WebsiteId', 'WebsiteId'
        );
    }
}
