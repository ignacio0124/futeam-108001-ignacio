<?php
/**
 * ReportMember Model
 *
 * @author 重甫
 * @since 05/06/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReportMember
 *
 * @package App\ReportMember
 */
class ReportMember extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ReportMember';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ReportMemberId';

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * 設定與Report關聯
     */
    public function Report() {
        return $this->hasOne(
            'App\Model\Report', 'ReportMemberId', 'ReportMemberId'
        );
    }
}
