<?php
/**
 * UserAccount Model
 *
 * @author 重甫
 * @since 03/21/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class UserAccount
 *
 * @package App\Model
 */
class UserAccount extends Authenticatable
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'UserAccount';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'UserAccountId';

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * 設定與UserWebsite關聯
     */
    public function UserWebsite() {
        return $this->hasManey(
            'App\Model\UserWebsite', 'UserAccountId', 'UserAccountId'
        );
    }
}
