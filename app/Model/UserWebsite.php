<?php
/**
 * UserWebsite Model
 *
 * @author 重甫
 * @since 06/03/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWebsite
 *
 * @package App\Model
 */
class UserWebsite extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'UserWebsite';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'UserWebsiteId';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['UserWebsiteId'];

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * 設定與UserAccount關聯
     */
    public function UserAccount() {
        return $this->belongsTo(
            'App\Model\UserAccount', 'UserAccountId', 'UserAccountId'
        );
    }

    /**
     * 設定與Website關聯
     */
    public function Website() {
        return $this->belongsTo(
            'App\Model\Website', 'WebsiteId', 'WebsiteId'
        );
    }

    /**
     * 設定與UserWebsiteContent關聯
     */
    public function UserWebsiteContent() {
        return $this->hasMany(
            'App\Model\UserWebsiteContent', 'UserWebsiteId', 'UserWebsiteId'
        );
    }
}
