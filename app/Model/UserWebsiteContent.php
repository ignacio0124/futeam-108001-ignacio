<?php
/**
 * UserWebsiteContent Model
 *
 * @author 重甫
 * @since 06/24/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWebsiteContent
 *
 * @package App\Model
 */
class UserWebsiteContent extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'UserWebsiteContent';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'UserWebsiteContentId';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['UserWebsiteContentId'];

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * 設定與UserWebsite關聯
     */
    public function UserWebsite() {
        return $this->belongsTo(
            'App\Model\UserWebsite', 'UserWebsiteId', 'UserWebsiteId'
        );
    }

    /**
     * 設定與UserWebsiteContentMember關聯
     */
    public function UserWebsiteContentMember() {
        return $this->hasMany(
            'App\Model\UserWebsiteContentMember', 'UserWebsiteContentId', 'UserWebsiteContentId'
        );
    }
}
