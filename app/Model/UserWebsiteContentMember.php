<?php
/**
 * UserWebsiteContentMember Model
 *
 * @author 重甫
 * @since 06/24/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserWebsiteContentMember
 *
 * @package App\Model
 */
class UserWebsiteContentMember extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'UserWebsiteContentMember';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'UserWebsiteContentMemberId';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['UserWebsiteContentMemberId'];

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * 設定與UserWebsiteContent關聯
     */
    public function UserWebsiteContent() {
        return $this->belongsTo(
            'App\Model\UserWebsiteContent', 'UserWebsiteContentId', 'UserWebsiteContentId'
        );
    }

    /**
     * 設定與Member關聯
     */
    public function MemberFromMember() {
        return $this->belongsTo(
            'App\Model\Member', 'MemberFrom', 'MemberId'
        )->withDefault();
    }

    /**
     * 設定與Member關聯
     */
    public function MemberToMember() {
        return $this->belongsTo(
            'App\Model\Member', 'MemberTo', 'MemberId'
        )->withDefault();
    }
}
