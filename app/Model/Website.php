<?php
/**
 * Website Model
 *
 * @author 重甫
 * @since 04/19/19
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Website
 *
 * @package App\Model
 */
class Website extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Website';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'WebsiteId';

    /**
     * The timestamps for the model.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * 設定與UserWebsite關聯
     */
    public function UserWebsite() {
        return $this->hasMany(
            'App\Model\UserWebsite', 'WebsiteId', 'WebsiteId'
        );
    }

    /**
     * 設定與Report關聯
     */
    public function Report() {
        return $this->hasMany(
            'App\Model\Report', 'WebsiteId', 'WebsiteId'
        );
    }
}
