<?php
/**
 * 主頁的Presenter
 *
 * @author 重甫
 * @since 06/25/19
 */
namespace App\Presenter;

/**
 * Class MainPresenter
 *
 * @package App\Presenter
 */
class MainPresenter
{
    /**
     * 取得流水編號
     *
     * @param  int $key           判斷的key值
     * @param  int $serial_number 流水編號
     * @param  int $user_website_content_id 儲存的網站id
     * @return string html語法
     */
    public function getSerialNumber($key, $serial_number, $user_website_content_id) {
        if ($key == 0) {
            return '<div class="table__body-cust-item">
                        <a class="table__body-cust-delAll">
                            <i onclick="deleteWebsiteContent(this, \'' . $user_website_content_id . '\')"></i>
                        </a>
                        <span>' . $serial_number . '</span>
                    </div>';
        } else {
            return '<div class="table__body-cust-item">
                        <a style="width: 20px;">
                            <i></i>
                        </a>
                        <span></span>
                    </div>';
        }
    }

    /**
     * 取得條件
     *
     * @param  int    $key 判斷的key值
     * @param  \Illuminate\Database\Eloquent\Collection $user_website_content 網站內容
     * @param  int    $row 第幾列
     * @return string html語法
     */
    public function getCondition($key, $user_website_content, $row) {
        if ($key == 0) {
            return '<div class="table__body-cust-li">
                        <div class="table__body-cust-content">
                            <div class="dropDown">
                                <div class="dropDown__select">
                                    <span>' . $user_website_content->ConditionX . '</span>
                                    <i class="table__body-content-toggle">
                                        <svg viewBox="0 0 10 10">
                                            <polygon points="2,1 8,1 5,6"></polygon>
                                            <polygon points="2,5 8,5 5,9"></polygon>
                                        </svg>
                                    </i>
                                </div>
                                <div name="crawler_dropdown_x_' . $row . '" group="x_' . $user_website_content->Group . '" class="dropDown__menu" style="display: none;">
                                    <div class="dropDown__content scrollbar-macosx">
                                    </div>
                                </div>
                            </div>
                            <div class="dropDown">
                                <div class="dropDown__select">
                                    <span>' . $user_website_content->ConditionY . '</span>
                                    <i class="table__body-content-toggle">
                                        <svg viewBox="0 0 10 10">
                                            <polygon points="2,1 8,1 5,6"></polygon>
                                            <polygon points="2,5 8,5 5,9"></polygon>
                                        </svg>
                                    </i>
                                </div>
                                <div name="crawler_dropdown_y_' . $row . '" group="y_' . $user_website_content->Group . '" class="dropDown__menu" style="display: none;">
                                    <div class="dropDown__content scrollbar-macosx">
                                    </div>
                                </div>
                            </div>
                            <a><i class="icn__calculator" onclick="viewPopupCalculate(' . $row . ', ' . $user_website_content->Group . ')"></i></a>
                            <input name="plus" type="hidden" value="' . $user_website_content->Plus . '" priority="' . $user_website_content->PlusPriority . '"/>
                            <input name="minus" type="hidden" value="' . $user_website_content->Minus . '" priority="' . $user_website_content->MinusPriority . '"/>
                            <input name="times" type="hidden" value="' . $user_website_content->Times . '" priority="' . $user_website_content->TimesPriority . '"/>
                            <input name="divided" type="hidden" value="' . $user_website_content->Divided . '" priority="' . $user_website_content->DividedPriority . '"/>
                        </div>
                    </div>';
        } else {
            return '<div class="table__body-cust-li">
                        <div class="table__body-cust-content"></div>
                    </div>';
        }
    }

    /**
     * 取得刪除的Icon
     *
     * @param  int $key   判斷的key值
     * @param  int $count 計數(判斷迴圈數量)
     * @param  int $group 群組
     * @param  int $row   第幾列
     * @return string html語法
     */
    public function getDeleteIcon($key, $count, $group, $row) {
        if ($key == 0) {
            return '<div class="table__body-cust-del" style="pointer-events: none;"></div>';
        } else if ($key == 1 && $count == 2) {
            return '<div class="table__body-cust-del" onclick="deleteRow(this, \'name\', ' . $row . ', ' . $group . ')" style="pointer-events: none;">
                        <svg viewBox="0 0 10 10" style="display: none;">
                            <path d="M1 1 L9 9"></path>
                            <path d="M9 1 L1 9"></path>
                        </svg>
                    </div>';
        } else {
            return '<div class="table__body-cust-del" onclick="deleteRow(this, \'name\', ' . $row . ', ' . $group . ')">
                        <svg viewBox="0 0 10 10">
                            <path d="M1 1 L9 9"></path>
                            <path d="M9 1 L1 9"></path>
                        </svg>
                    </div>';
        }
    }

    /**
     * 取得按鈕
     *
     * @param  int $key   判斷的key值
     * @param  int $count 計數(判斷迴圈數量)
     * @param  int $group 群組
     * @param  int $row   第幾列
     * @param  \Illuminate\Database\Eloquent\Collection $member 成員
     * @return string html語法
     */
    public function getButton($key, $count, $group, $row, $member) {
        if ($key == 0) {
            return '<div class="table__body-cust-item">
                        <a class="table__body-cust-add" onclick="newRow(\'name\', ' . $row . ', ' . $group . ', '. htmlspecialchars($member) . ')">
                            <svg viewBox="0 0 10 10">
                                <path d="M2 5 L8 5"></path>
                                <path d="M5 2 L5 8"></path>
                            </svg>
                        </a>
                    </div>';
        } else if ($key == $count - 1) {
            return '<div class="table__body-cust-item">
                        <a name="confirm_' . $group . '" class="table__body-cust-btn" onclick="setReportInformation(' . $row . ', ' . $group . ')">確認</a>
                    </div>';
        } else {
            return '<div class="table__body-cust-item"></div>';
        }

    }

    /**
     * 取得預設
     *
     * @param  int $count 計數(判斷迴圈數量)
     * @param  int $row   第幾列
     * @param  \Illuminate\Database\Eloquent\Collection $member 成員
     * @param  string $kind 種類
     * @return string html語法
     */
    public function getDefault($count, $row, $member, $kind) {
        /** @var string 成員選單預設值 */
        $member_list = "";
        foreach ($member as $_member) {
            $member_list .= '<li class="match__item" onclick="setName(this, \'' . $_member["Name"] . '\', ' . $_member["MemberId"] . ')">
                                 <div class="match__item-content">
                                     <span>' . $_member["Name"] . '</span>
                                 </div>
                             </li>';
        }
        switch ($kind) {
            case "out":
                if ($count == 0) {
                    return '<div name="group_1" class="table__body-cust">
                                <div class="table__body-cust-row">
                                    <div class="table__body-cust-item">
                                        <a style="width: 20px;">
                                            <i></i>
                                        </a>
                                        <span>1</span>
                                    </div>
                                    <div class="table__body-cust-item">
                                        <div class="table__body-cust-li">
                                            <div class="table__body-cust-content">
                                                <div class="dropDown">
                                                    <div class="dropDown__select">
                                                        <span></span>
                                                        <i class="table__body-content-toggle">
                                                            <svg viewBox="0 0 10 10">
                                                                <polygon points="2,1 8,1 5,6"></polygon>
                                                                <polygon points="2,5 8,5 5,9"></polygon>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div name="crawler_dropdown_x_' . $row . '" group="x_1" class="dropDown__menu" style="display: none;">
                                                        <div class="dropDown__content scrollbar-macosx">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dropDown">
                                                    <div class="dropDown__select">
                                                        <span></span>
                                                        <i class="table__body-content-toggle">
                                                            <svg viewBox="0 0 10 10">
                                                                <polygon points="2,1 8,1 5,6"></polygon>
                                                                <polygon points="2,5 8,5 5,9"></polygon>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div name="crawler_dropdown_y_' . $row . '" group="y_1" class="dropDown__menu" style="display: none;">
                                                        <div class="dropDown__content scrollbar-macosx">
                                                        </div>
                                                    </div>
                                                </div>
                                                <a><i class="icn__calculator" onclick="viewPopupCalculate(' . $row . ', 1)"></i></a>
                                                <input name="plus" type="hidden" value="" priority=""/>
                                                <input name="minus" type="hidden" value="" priority=""/>
                                                <input name="times" type="hidden" value="" priority=""/>
                                                <input name="divided" type="hidden" value="" priority=""/>
                                            </div>
                                        </div>
                                        <div name="first" class="table__body-cust-li">
                                            <span>姓名 / %數:</span>
                                            <div class="table__body-cust-content">
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                                    <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                    <div class="match">
                                                        <div class="match_addItem">
                                                            <input type="text" placeholder="新增" />
                                                            <a>
                                                                <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                    <path d="M5 2 L5 8"></path>
                                                                    <path d="M2 5 L8 5"></path>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                            ' . $member_list . '
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="輸入%數" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table__body-cust-li">
                                            <div class="table__body-cust-content">
                                                <i>
                                                    <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                                        <polygon points="2,1 8,1 5,6"></polygon>
                                                        <polygon points="2,5 8,5 5,9"></polygon>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div name="second" class="table__body-cust-li">
                                            <span>姓名 / %數:</span>
                                            <div class="table__body-cust-content">
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                                    <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                    <div class="match">
                                                        <div class="match_addItem">
                                                            <input type="text" placeholder="新增" />
                                                            <a>
                                                                <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                    <path d="M5 2 L5 8"></path>
                                                                    <path d="M2 5 L8 5"></path>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                            ' . $member_list . '
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="輸入%數" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table__body-cust-del" style="pointer-events: none;">
                                        </div>
                                    </div>
                                    <div class="table__body-cust-item">
                                        <a class="table__body-cust-add" onclick="newRow(\'name\', ' . $row . ', 1, ' . htmlspecialchars($member) . ')">
                                            <svg viewBox="0 0 10 10">
                                                <path d="M2 5 L8 5"></path>
                                                <path d="M5 2 L5 8"></path>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                                <div class="table__body-cust-row">
                                    <div class="table__body-cust-item"></div>
                                    <div class="table__body-cust-item">
                                        <div class="table__body-cust-li">
                                            <div class="table__body-cust-content">
                                                <!-- <input type="text" /> -->
                                            </div>
                                        </div>
                                        <div name="first" class="table__body-cust-li">
                                            <span>姓名 / %數:</span>
                                            <div class="table__body-cust-content">
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                                    <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                    <div class="match">
                                                        <div class="match_addItem">
                                                            <input type="text" placeholder="新增" />
                                                            <a>
                                                                <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                    <path d="M5 2 L5 8"></path>
                                                                    <path d="M2 5 L8 5"></path>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                            ' . $member_list . '
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="輸入%數" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table__body-cust-li">
                                            <div class="table__body-cust-content">
                                                <i>
                                                    <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                                        <polygon points="2,1 8,1 5,6"></polygon>
                                                        <polygon points="2,5 8,5 5,9"></polygon>
                                                    </svg>
                                                </i>
                                            </div>
                                        </div>
                                        <div name="second" class="table__body-cust-li">
                                            <span>姓名 / %數:</span>
                                            <div class="table__body-cust-content">
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                                    <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                    <div class="match">
                                                        <div class="match_addItem">
                                                            <input type="text" placeholder="新增" />
                                                            <a>
                                                                <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                    <path d="M5 2 L5 8"></path>
                                                                    <path d="M2 5 L8 5"></path>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                            ' . $member_list . '
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-input">
                                                    <input type="text" placeholder="輸入%數" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table__body-cust-del" onclick="deleteRow(this, \'name\', ' . $row . ', 1)" style="pointer-events: none;">
                                            <svg viewBox="0 0 10 10" style="display: none;">
                                                <path d="M1 1 L9 9"></path>
                                                <path d="M9 1 L1 9"></path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="table__body-cust-item">
                                        <a name="confirm_1" class="table__body-cust-btn" onclick="setReportInformation(' . $row . ', 1)">確認</a>
                                    </div>
                                </div>
                            </div>';
                }
                break;
            case "in":
                if ($count == 1) {
                    return '<div class="table__body-cust-row">
                                <div class="table__body-cust-item"></div>
                                <div class="table__body-cust-item">
                                    <div class="table__body-cust-li">
                                        <div class="table__body-cust-content">
                                            <!-- <input type="text" /> -->
                                        </div>
                                    </div>
                                    <div name="first" class="table__body-cust-li">
                                        <span>姓名 / %數:</span>
                                        <div class="table__body-cust-content">
                                            <div class="table__body-cust-input">
                                                <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                <div class="match">
                                                    <div class="match_addItem">
                                                        <input type="text" placeholder="新增" />
                                                        <a>
                                                            <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                <path d="M5 2 L5 8"></path>
                                                                <path d="M2 5 L8 5"></path>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                        ' . $member_list . '
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table__body-cust-input">
                                                <input type="text" placeholder="輸入%數" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table__body-cust-li">
                                        <div class="table__body-cust-content">
                                            <i>
                                                <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                                    <polygon points="2,1 8,1 5,6"></polygon>
                                                    <polygon points="2,5 8,5 5,9"></polygon>
                                                </svg>
                                            </i>
                                        </div>
                                    </div>
                                    <div name="second" class="table__body-cust-li">
                                        <span>姓名 / %數:</span>
                                        <div class="table__body-cust-content">
                                            <div class="table__body-cust-input">
                                                <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                <div class="match">
                                                    <div class="match_addItem">
                                                        <input type="text" placeholder="新增" />
                                                        <a>
                                                            <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                <path d="M5 2 L5 8"></path>
                                                                <path d="M2 5 L8 5"></path>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                        ' . $member_list . '
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table__body-cust-input">
                                                <input type="text" placeholder="輸入%數" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table__body-cust-del" onclick="deleteRow(this, \'name\', ' . $row . ', 1)" style="pointer-events: none;">
                                        <svg viewBox="0 0 10 10" style="display: none;">
                                            <path d="M1 1 L9 9"></path>
                                            <path d="M9 1 L1 9"></path>
                                        </svg>
                                    </div>
                                </div>
                                <div class="table__body-cust-item">
                                    <a name="confirm_1" class="table__body-cust-btn" onclick="setReportInformation(' . $row . ', 1)">確認</a>
                                </div>
                            </div>';
                }
                break;
        }
    }
}
