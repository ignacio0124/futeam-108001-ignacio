<?php
/**
 * 計算彈跳視窗的Presenter
 *
 * @author 重甫
 * @since 08/01/19
 */
namespace App\Presenter;

/**
 * Class PopupCalculatePresenter
 *
 * @package App\Presenter
 */
class PopupCalculatePresenter
{
    /**
     * 取得計算的列
     * @param  array $calculate 要計算的值
     * @param  array $column    撈取的欄位值
     * @param  int   $row       第幾列
     * @param  int   $group     第幾群組
     * @return string html語法
     */
    public function getCalculateRow($calculate, $column, $row, $group) {
        /** @var array 優先順序 */
        $priority = [is_null($calculate["plus_priority"]) ? 99 : $calculate["plus_priority"] => "plus",
                     is_null($calculate["minus_priority"]) ? 99 : $calculate["minus_priority"] => "minus",
                     is_null($calculate["times_priority"]) ? 99 : $calculate["times_priority"] => "times",
                     is_null($calculate["divided_priority"]) ? 99 : $calculate["divided_priority"] => "divided"];
        /** 排序 */
        ksort($priority);
        /** @var string 要回傳的html */
        $html = "";

        for ($i = 0; $i < 4; $i ++) {
            if (empty($priority[$i])) {
                break;
            } else if (isset($priority[$i])) {
                /** @var array 要計算的值切割成陣列 */
                $calculate_arr = explode(" ", $calculate[$priority[$i]]);
                /** @var int 要計算的值的陣列長度 */
                $calculate_count = count($calculate_arr);
                /** @var string 下拉選單 */
                $menu = "";
                if (isset($column)) {
                    foreach($column as $key => $_column) {
                        if ($calculate_count == 1) {
                            if ($calculate_arr[0] == $_column) {
                                $menu .= '<label class="dropDown__checkbox" name="popup" for="' . $priority[$i] . $key . '">
                                             <input type="checkbox" checked="checked" onclick="setCalculateValue(\'' . $_column . '\', this)" id="' . $priority[$i] . $key . '" />
                                             <div class="checkbox"></div>
                                             <span>' . $_column . '</span>
                                          </label>';
                            } else {
                                $menu .= '<label class="dropDown__checkbox" name="popup" for="' . $priority[$i] . $key . '">
                                             <input type="checkbox" onclick="setCalculateValue(\'' . $_column . '\', this)" id="' . $priority[$i] . $key . '" />
                                             <div class="checkbox"></div>
                                             <span>' . $_column . '</span>
                                          </label>';
                            }
                        } else if ($calculate_count > 1) {
                            /** @var int 判斷是否一樣 */
                            $same = 0;
                            foreach ($calculate_arr as $_calculate_arr) {
                                if ($_calculate_arr == $_column) {
                                    $same = 1;
                                }
                            }

                            if ($same == 1) {
                                $menu .= '<label class="dropDown__checkbox" name="popup" for="' . $priority[$i] . $key . '">
                                             <input type="checkbox" checked="checked" onclick="setCalculateValue(\'' . $_column . '\', this)" id="' . $priority[$i] . $key . '" />
                                             <div class="checkbox"></div>
                                             <span>' . $_column . '</span>
                                          </label>';
                            } else if ($same == 0) {
                                $menu .= '<label class="dropDown__checkbox" name="popup" for="' . $priority[$i] . $key . '">
                                             <input type="checkbox" onclick="setCalculateValue(\'' . $_column . '\', this)" id="' . $priority[$i] . $key . '" />
                                             <div class="checkbox"></div>
                                             <span>' . $_column . '</span>
                                          </label>';
                            }
                        }
                    }
                }

                /**
                 * 判斷點選的符號
                 *
                 * @param {string} symbol 符號
                 */
                switch ($priority[$i]) {
                    case "plus":
                        $symbol = "+";
                        break;
                    case "minus":
                        $symbol = "-";
                        break;
                    case "times":
                        $symbol = "*";
                        break;
                    case "divided":
                        $symbol = "÷";
                        break;
                    default:
                        $symbol = "";
                        break;
                }
                /** @var int 流水號 */
                $serial = $i + 1;

                $html .= '<div name="' . $priority[$i] . '" class="popup__item">
                             <div class="popup__item-number">' . $serial . '</div>
                             <div class="popup__item-content">
                                 <div class="dropDown">
                                     <div class="dropDown__select" onclick="setDropDown(this)">
                                         <span>(' . $symbol . ') ' . $calculate[$priority[$i]] . '</span>
                                         <i>
                                             <svg viewBox="0 0 10 10" class="triangle">
                                                 <polygon points="2,1 8,1 5,6"></polygon>
                                                 <polygon points="2,5 8,5 5,9"></polygon>
                                             </svg>
                                         </i>
                                     </div>
                                     <div class="dropDown__menu" style="display: none;">
                                         <div class="dropDown__content scrollbar-macosx">
                                             ' . $menu . '
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             <a class="popup__item-btn" onclick="deleteCalculateRow(this, \'' . $priority[$i] . '\', ' . $row . ', ' . $group . ')">
                                 <svg viewBox="0 0 10 10">
                                     <path d="M2 2 L8 8"></path>
                                     <path d="M8 2 L2 8"></path>
                                 </svg>
                             </a>
                         </div>';
            }
        }

        return $html;
    }
}
