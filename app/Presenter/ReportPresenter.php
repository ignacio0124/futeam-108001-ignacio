<?php
/**
 * 報表的Presenter
 *
 * @author 重甫
 * @since 05/10/19
 */
namespace App\Presenter;

/**
 * Class ReportPresenter
 *
 * @package App\Presenter
 */
class ReportPresenter
{
    /**
     * 取得每個成員的錢
     *
     * @param  \Illuminate\Database\Eloquent\Collection $user_website   使用者擁有的網站
     * @param  \Illuminate\Database\Eloquent\Collection $report_website 網站資訊
     * @param  \Illuminate\Database\Eloquent\Collection $member         成員
     * @param  string $date 日期
     * @return string html語法
     */
    public function getMemberMoney($user_website, $report_website, $member, $date) {
        /** @var string 要回傳的html預設 */
        $html = "";
        /** @var \Illuminate\Database\Eloquent\Collection 群組後的網站 */
        $group_website = $user_website->Website->Report->where("Date", '=', $date)->unique("Group");
        /** 群組 */
        foreach ($group_website as $key => $_group_website) {
            $html .= '<div class="board__row">';
            /** 成員 */
            foreach ($member as $_member) {
                /** @var int 總金額的預設 */
                $money = 0;
                foreach ($report_website as $_report_website) {
                    /** 判斷相同的群組 */
                    if ($_report_website["Group"] == $_group_website["Group"] && $_report_website["WebsiteId"] == $_group_website["WebsiteId"]) {
                        /** 判斷對應的人名及錢 */
                        if ($_report_website->ReportMember->MemberFrom == $_member["MemberId"]) {
                            $money += $_report_website->ReportMember->MoneyFrom;
                        } else if ($_report_website->ReportMember->MemberTo == $_member["MemberId"]) {
                            $money += $_report_website->ReportMember->MoneyTo;
                        }
                    }
                }
                if ($money == 0) {
                    $html .= '<div name="' . $_member["Number"] . '" class="board__item board__item_positive">0</div>';
                } else if ($money > 0) {
                    $html .= '<div name="' . $_member["Number"] . '" class="board__item board__item_positive">' . $money . '</div>';
                } else if ($money < 0) {
                    $html .= '<div name="' . $_member["Number"] . '" class="board__item board__item_negative">' . $money . '</div>';
                }
            }
            $html .= '</div>';
        }
        return $html;
    }

    /**
     * 取得報表的網站
     *
     * @param  \Illuminate\Database\Eloquent\Collection $user_website 使用者擁有的網站
     * @param  string $date 日期
     * @return string html語法
     */
    public function getReportWebsite($user_website, $date) {
        /** @var \Illuminate\Database\Eloquent\Collection 群組後的網站 */
        $group_website = $user_website->Website->Report->where("Date", '=', $date)->unique("Group");
        /** @var string 網站名稱 */
        $website = $user_website->Website["Name"];
        /** @var string 要回傳的html預設 */
        $html = "";

        foreach ($group_website as $_group_website) {
            $html .= '<div class="board__row">
                          <div class="board__item"></div>
                          <div class="board__item">' . $website . '(' . $_group_website["Account"] . ')</div>
                          <div class="board__item">' . $_group_website["Name"] .'</div>
                      </div>';
        }
        return $html;
    }
}
