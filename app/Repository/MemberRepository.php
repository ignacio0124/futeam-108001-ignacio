<?php
/**
 * UserAccount Repository
 *
 * @author 重甫
 * @since 04/23/19
 */

namespace App\Repository;

use DB;
use App\Model\Member;
use Illuminate\Support\Facades\Auth;

/**
 * Class MemberRepository
 *
 * @package App\Repository
 */
class MemberRepository
{
    /**
     * 注入的參數
     *
     * @var Member
     */
    protected $Member;

    /**
     * MemberRepository constructor
     *
     * @param Member $member
     */
    public function __construct(Member $member) {
        $this->Member = $member;
    }

    /**
     * 取得自己的成員
     *
     * @return \Illuminate\Database\Eloquent\Collection 成員
     */
    public function getMember() {
        return $this->Member->where('UserAccountId', '=', Auth::user()->UserAccountId)->get();
    }

    /**
     * 取得目前最大的編號
     *
     * @return int 目前最大的編號
     */
    public function getMaxNumber() {
        return $this->Member->where('UserAccountId', '=', Auth::user()->UserAccountId)->max('Number');
    }

    /**
     * 新增成員
     *
     * @param  string $Name    人名
     * @param  string $Account 帳號
     * @return int 新增後取得的id
     */
    public function newMember($Name, $Account) {
        /** @var int 目前最大的編號 */
        $Number = self::getMaxNumber();

        return $this->Member->insertGetId(['Number' => $Number + 1,
                                           'Name' => $Name,
                                           'Account' => $Account,
                                           'UserAccountId' => Auth::user()->UserAccountId]);
    }

    /**
     * 刪除成員
     *
     * @param  int $MemberId MemberId
     * @return int 1=成功,0=失敗
     */
    public function deleteMember($MemberId) {
        return $this->Member->where('MemberId', '=', $MemberId)->delete();
    }

    /**
     * 更新成員資訊
     *
     * @param int    $MemberId MemberId
     * @param string $Name     人名
     * @param string $Account  帳號
     * @return int 1=成功,0=失敗
     */
    public function setMember($MemberId, $Name, $Account) {
        return $this->Member->where('MemberId', '=', $MemberId)
                            ->update(['Name' => $Name,
                                      'Account' => $Account]);
    }
}
