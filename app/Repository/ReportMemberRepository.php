<?php
/**
 * ReportMember Repository
 *
 * @author 重甫
 * @since 05/06/19
 */

namespace App\Repository;

use DB;
use App\Model\ReportMember;
use Illuminate\Support\Facades\Auth;

/**
 * Class ReportMemberRepository
 *
 * @package App\Repository
 */
class ReportMemberRepository
{
    /**
     * 注入的參數
     *
     * @var ReportMember
     */
    protected $ReportMember;

    /**
     * ReportMemberRepository constructor
     *
     * @param ReportMember $reportMember
     */
    public function __construct(ReportMember $reportMember) {
        $this->ReportMember = $reportMember;
    }

    /**
     * 新增一筆資料並取得ID
     *
     * @param  int $MemberFrom 成員1
     * @param  int $MoneyFrom  錢1
     * @param  int $MemberTo   成員2
     * @param  int $MoneyTo    錢2
     * @return int ReportMemberId
     */
    public function newOneGetId($MemberFrom, $MoneyFrom, $MemberTo, $MoneyTo) {
        return $this->ReportMember->insertGetId(['MemberFrom' => $MemberFrom,
                                                 'MoneyFrom' => $MoneyFrom,
                                                 'MemberTo' => $MemberTo,
                                                 'MoneyTo' => $MoneyTo]);
    }

    /**
     * 更新成員的錢
     *
     * @param  int $MoneyFrom  錢1
     * @param  int $MoneyTo    錢2
     * @param  int $ReportMemberId 報表成員ID
     * @return int 更新結果
     */
    public function setMemberMoney($MoneyFrom, $MoneyTo, $ReportMemberId) {
        return $this->ReportMember->where('ReportMemberId', '=', $ReportMemberId)
                                  ->update(['MoneyFrom' => $MoneyFrom,
                                            'MoneyTo' => $MoneyTo]);
    }

    /**
     * 刪除成員
     *
     * @param  int $ReportMemberId 報表成員ID
     * @return int 1=成功,0=失敗
     */
    public function deleteMember($ReportMemberId) {
        return $this->ReportMember->where('ReportMemberId', '=', $ReportMemberId)->delete();
    }
}
