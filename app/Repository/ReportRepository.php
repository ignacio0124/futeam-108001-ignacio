<?php
/**
 * Report Repository
 *
 * @author 重甫
 * @since 05/06/19
 */

namespace App\Repository;

use DB;
use App\Model\Report;
use Illuminate\Support\Facades\Auth;

/**
 * Class ReportRepository
 *
 * @package App\Repository
 */
class ReportRepository
{
    /**
     * 注入的參數
     *
     * @var Report
     */
    protected $Report;

    /**
     * ReportRepository constructor
     *
     * @param Report $report
     */
    public function __construct(Report $report) {
        $this->Report = $report;
    }

    /**
     * 取得有報表的網站資料
     *
     * @param  string $Date 日期區間
     * @return \Illuminate\Database\Eloquent\Builder 取得的網站資料
     */
    public function getReportWebsite() {
        return $this->Report->where('UserAccountId', '=', Auth::user()->UserAccountId)->orderBy('Date', 'desc');
    }

    /**
     * 取得報表成員ID
     *
     * @param  int    $UserAccountId  使用者ID
     * @param  int    $WebsiteId      網站ID
     * @param  string $Account        帳號
     * @param  string $Name           計算欄位金額的人名
     * @param  int    $Group          群組編號
     * @param  int    $ReportMemberId 報表成員資料表ID
     * @param  string $Date           日期
     * @param  int    $MemberFrom     成員1
     * @param  int    $MemberTo       成員2
     * @return int ReportMemberId
     */
    public function getReportMemberId($UserAccountId, $WebsiteId, $Account, $Name, $Group, $Date, $MemberFrom, $MemberTo) {
        return $this->Report->select('Report.ReportMemberId')
                            ->join('ReportMember', 'Report.ReportMemberId', 'ReportMember.ReportMemberId')
                            ->where(['Report.UserAccountId' => $UserAccountId,
                                     'Report.WebsiteId' => $WebsiteId,
                                     'Report.Account' => $Account,
                                     'Report.Name' => $Name,
                                     'Report.Group' => $Group,
                                     'Report.Date' => $Date,
                                     'ReportMember.MemberFrom' => $MemberFrom,
                                     'ReportMember.MemberTo' => $MemberTo])->value('ReportMemberId');
    }

    /**
     * 取得報表成員ID
     *
     * @param  int $UserWebsiteContentId 成員內容ID
     * @return int ReportMemberId
     */
    public function getReportMemberIdFromUserWebsiteContentId($UserWebsiteContentId) {
        return $this->Report->where('UserWebsiteContentId', '=', $UserWebsiteContentId)->value('ReportMemberId');
    }

    /**
     * 新增一筆資料
     *
     * @param  int    $UserAccountId  使用者ID
     * @param  int    $WebsiteId      網站ID
     * @param  string $Account        帳號
     * @param  string $Name           計算欄位金額的人名
     * @param  int    $Group          群組編號
     * @param  int    $ReportMemberId 報表成員資料表ID
     * @param  int    $UserWebsiteContentId 成員內容ID
     * @param  string $Date           日期
     * @return int 新增結果
     */
    public function newOne($UserAccountId, $WebsiteId, $Account, $Name, $Group, $ReportMemberId, $UserWebsiteContentId, $Date) {
        return $this->Report->insert(['UserAccountId' => $UserAccountId,
                                      'WebsiteId' => $WebsiteId,
                                      'Account' => $Account,
                                      'Name' => $Name,
                                      'Group' => $Group,
                                      'ReportMemberId' => $ReportMemberId,
                                      'UserWebsiteContentId' => $UserWebsiteContentId,
                                      'Date' => $Date]);
    }

    /**
     * 刪除一筆資料
     *
     * @param  int $UserWebsiteContentId 成員內容ID
     * @return int 1=成功,0=失敗
     */
    public function deleteOne($UserWebsiteContentId) {
        return $this->Report->where('UserWebsiteContentId', '=', $UserWebsiteContentId)->delete();
    }
}
