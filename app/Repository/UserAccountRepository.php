<?php
/**
 * UserAccount Repository
 *
 * @author 重甫
 * @since 03/21/19
 */

namespace App\Repository;

use DB;
use App\Model\UserAccount;

/**
 * Class UserAccountRepository
 *
 * @package App\Repository
 */
class UserAccountRepository
{
    /**
     * 注入的參數
     *
     * @var UserAccount
     */
    protected $UserAccount;

    /**
     * UserAccountRepository constructor
     *
     * @param UserAccount $userAccount
     */
    public function __construct(UserAccount $userAccount) {
        $this->UserAccount = $userAccount;
    }

    /**
     * 取得對應帳號的用戶
     *
     * @param mixed $account 帳號
     * @return \Illuminate\Database\Eloquent\Model 對應帳號的用戶
     */
    public function getUser($account) {
        return $this->UserAccount->where('Account', '=', $account)->first();
    }
}
