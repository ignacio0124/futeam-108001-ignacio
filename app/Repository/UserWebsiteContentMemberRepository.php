<?php
/**
 * UserWebsiteContentMember Repository
 *
 * @author 重甫
 * @since 06/25/19
 */

namespace App\Repository;

use DB;
use App\Model\UserWebsiteContentMember;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserWebsiteContentMemberRepository
 *
 * @package App\Repository
 */
class UserWebsiteContentMemberRepository
{
    /**
     * 注入的參數
     *
     * @var UserWebsiteContentMember
     */
    protected $UserWebsiteContentMember;

    /**
     * UserWebsiteContentMemberRepository constructor
     *
     * @param UserWebsiteContentMember $userWebsiteContentMember
     */
    public function __construct(UserWebsiteContentMember $userWebsiteContentMember) {
        $this->UserWebsiteContentMember = $userWebsiteContentMember;
    }

    /**
     * 新增使用者儲存的網站內容
     *
     * @param int    $UserWebsiteContentId 使用者擁有的網站內容ID
     * @param int    $MemberFrom           成員
     * @param string $PercentFrom          %數
     * @param int    $MemberTo             成員
     * @param string $PercentTo            %數
     * @return int 1=成功, 0=失敗
     */
    public function newMemberContent($UserWebsiteContentId, $MemberFrom, $PercentFrom, $MemberTo, $PercentTo) {
        return $this->UserWebsiteContentMember->insert(['UserWebsiteContentId' => $UserWebsiteContentId,
                                                        'MemberFrom' => $MemberFrom,
                                                        'PercentFrom' => $PercentFrom,
                                                        'MemberTo' => $MemberTo,
                                                        'PercentTo' => $PercentTo]);
    }

    /**
     * 刪除網站成員內容
     *
     * @param int $UserWebsiteContentId 使用者擁有的網站內容ID
     * @return int 1=成功, 0=失敗
     */
    public function deleteMemberContent($UserWebsiteContentId) {
        return $this->UserWebsiteContentMember->where('UserWebsiteContentId', '=', $UserWebsiteContentId)->delete();
    }
}
