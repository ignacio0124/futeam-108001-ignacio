<?php
/**
 * UserWebsiteContent Repository
 *
 * @author 重甫
 * @since 06/25/19
 */

namespace App\Repository;

use DB;
use App\Model\UserWebsiteContent;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserWebsiteContentRepository
 *
 * @package App\Repository
 */
class UserWebsiteContentRepository
{
    /**
     * 注入的參數
     *
     * @var UserWebsiteContent
     */
    protected $UserWebsiteContent;

    /**
     * UserWebsiteContentRepository constructor
     *
     * @param UserWebsiteContent $userWebsiteContent
     */
    public function __construct(UserWebsiteContent $userWebsiteContent) {
        $this->UserWebsiteContent = $userWebsiteContent;
    }

    /**
     * 取得使用者儲存的網站內容ID
     *
     * @param array $website_content 使用者擁有的網站資訊
     * @return int 網站內容ID
     */
    public function getUserWebsiteContentId($website_content) {
        /**
         * 欄位值
         *
         * @var int $UserWebsiteId 網站id
         * @var int $ConditionX 條件
         * @var int $ConditionY 條件
         * @var int $Group 第幾群組
         * @var int $Plus 加法的值
         * @var int $PlusPriority 加法優先順序
         * @var int $Minus 減法的值
         * @var int $MinusPriority 減法優先順序
         * @var int $Times 乘法的值
         * @var int $TimesPriority 乘法優先順序
         * @var int $Divided 除法的值
         * @var int $DividedPriority 除法優先順序
         */
        $UserWebsiteId   = $website_content["user_website_id"];
        $ConditionX      = $website_content["condition_x"];
        $ConditionY      = $website_content["condition_y"];
        $Group           = $website_content["group"];
        $Plus            = is_null($website_content["plus"]) ? "" : $website_content["plus"];
        $PlusPriority    = $website_content["plus_priority"];
        $Minus           = is_null($website_content["minus"]) ? "" : $website_content["minus"];
        $MinusPriority   = $website_content["minus_priority"];
        $Times           = is_null($website_content["times"]) ? "" : $website_content["times"];
        $TimesPriority   = $website_content["times_priority"];
        $Divided         = is_null($website_content["divided"]) ? "" : $website_content["divided"];
        $DividedPriority = $website_content["divided_priority"];

        return $this->UserWebsiteContent->where(['UserWebsiteId' => $UserWebsiteId,
                                                 'ConditionX' => $ConditionX,
                                                 'ConditionY' => $ConditionY,
                                                 'Plus' => $Plus,
                                                 'PlusPriority' => $PlusPriority,
                                                 'Minus' => $Minus,
                                                 'MinusPriority' => $MinusPriority,
                                                 'Times' => $Times,
                                                 'TimesPriority' => $TimesPriority,
                                                 'Divided' => $Divided,
                                                 'DividedPriority' => $DividedPriority,
                                                 'Group' => $Group])->value('UserWebsiteContentId');
    }


    /**
     * 新增或更新使用者儲存的網站內容
     *
     * @param array $website_content 使用者擁有的網站資訊
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function setUserWebsiteContent($website_content) {
        /**
         * 欄位值
         *
         * @var int $UserWebsiteId 網站id
         * @var int $ConditionX 條件
         * @var int $ConditionY 條件
         * @var int $Group 第幾群組
         * @var int $Plus 加法的值
         * @var int $PlusPriority 加法優先順序
         * @var int $Minus 減法的值
         * @var int $MinusPriority 減法優先順序
         * @var int $Times 乘法的值
         * @var int $TimesPriority 乘法優先順序
         * @var int $Divided 除法的值
         * @var int $DividedPriority 除法優先順序
         */
        $UserWebsiteId   = $website_content["user_website_id"];
        $ConditionX      = $website_content["condition_x"];
        $ConditionY      = $website_content["condition_y"];
        $Group           = $website_content["group"];
        $Plus            = is_null($website_content["plus"]) ? "" : $website_content["plus"];
        $PlusPriority    = $website_content["plus_priority"];
        $Minus           = is_null($website_content["minus"]) ? "" : $website_content["minus"];
        $MinusPriority   = $website_content["minus_priority"];
        $Times           = is_null($website_content["times"]) ? "" : $website_content["times"];
        $TimesPriority   = $website_content["times_priority"];
        $Divided         = is_null($website_content["divided"]) ? "" : $website_content["divided"];
        $DividedPriority = $website_content["divided_priority"];

        return $this->UserWebsiteContent->updateOrCreate(
            ['UserWebsiteId' => $UserWebsiteId, 'ConditionX' => $ConditionX, 'ConditionY' => $ConditionY, 'Plus' => $Plus, 'PlusPriority' => $PlusPriority, 'Minus' => $Minus, 'MinusPriority' => $MinusPriority, 'Times' => $Times, 'TimesPriority' => $TimesPriority, 'Divided' => $Divided, 'DividedPriority' => $DividedPriority],
            ['Group' => $Group]
        );
    }

    /**
     * 刪除網站內容
     *
     * @param  int $UserWebsiteContentId 使用者儲存網站內容的id
     * @return int 1=成功,0=失敗
     */
    public function deleteWebsiteContent($UserWebsiteContentId) {
        return $this->UserWebsiteContent->where("UserWebsiteContentId", '=', $UserWebsiteContentId)->delete();
    }
}
