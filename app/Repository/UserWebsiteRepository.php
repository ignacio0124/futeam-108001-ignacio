<?php
/**
 * UserWebsite Repository
 *
 * @author 重甫
 * @since 06/03/19
 */

namespace App\Repository;

use DB;
use App\Model\UserWebsite;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserWebsiteRepository
 *
 * @package App\Repository
 */
class UserWebsiteRepository
{
    /**
     * 注入的參數
     *
     * @var UserWebsite
     */
    protected $UserWebsite;

    /**
     * UserWebsiteRepository constructor
     *
     * @param UserWebsite $userWebsite
     */
    public function __construct(UserWebsite $userWebsite) {
        $this->UserWebsite = $userWebsite;
    }

    /**
     * 取得使用者擁有的網站
     *
     * @return \Illuminate\Database\Eloquent\Collection 網站資訊
     */
    public function getUserWebsite() {
        return $this->UserWebsite->where('UserAccountId', '=', Auth::user()->UserAccountId)->get();
    }

    /**
     * 新增或更新使用者擁有的網站
     *
     * @param int    $WebsiteId WebsiteId
     * @param string $Account   帳號
     * @param string $Password  密碼
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function setUserWebsite($WebsiteId, $Account, $Password) {
        return $this->UserWebsite->updateOrCreate(['WebsiteId' => $WebsiteId,
                                                   'Account' => $Account,
                                                   'Password' => $Password,
                                                   'UserAccountId' => Auth::user()->UserAccountId]);
    }

}
