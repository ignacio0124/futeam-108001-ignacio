<?php
/**
 * Website Repository
 *
 * @author 重甫
 * @since 04/09/19
 */

namespace App\Repository;

use DB;
use App\Model\Website;

/**
 * Class WebsiteRepository
 *
 * @package App\Repository
 */
class WebsiteRepository
{
    /**
     * 注入的參數
     *
     * @var Website
     */
    protected $Website;

    /**
     * WebsiteRepository constructor
     *
     * @param Website $website
     */
    public function __construct(Website $website) {
        $this->Website = $website;
    }

    /**
     * 取得全部的網站
     *
     * @return \Illuminate\Database\Eloquent\Collection 全部的網站
     */
    public function getAllWebsite() {
        return $this->Website->all();
    }
}
