<?php
/**
 * 爬蟲相關的Service
 *
 * @author 重甫
 * @since 03/27/19
 */
namespace App\Service;

/**
 * Class CrawlerService
 *
 * @package App\Service
 */
class CrawlerService
{
    /**
     * 呼叫爬蟲
     *
     * @param  string $url      網址
     * @param  mixed  $account  帳號
     * @param  mixed  $password 密碼
     * @param  string $date1    起始日期
     * @param  string $date2    結束日期
     * @return array 爬蟲抓取的欄位值
     */
    public function callCrawler($url, $account, $password, $date1, $date2) {
        $ch = curl_init();
        curl_setopt(
            $ch,
            CURLOPT_URL,
            env('CRAWER_HOST').'/'.$url.'?account='.$account.'&password='.$password.'&date1='.$date1.'&date2='.$date2
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
        curl_exec($ch);

        $result = json_decode(curl_exec($ch));

        curl_close($ch);

        return $result;
    }
}
