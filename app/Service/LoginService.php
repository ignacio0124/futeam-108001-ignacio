<?php
/**
 * 登入的Service
 *
 * @author 重甫
 * @since 03/21/19
 */
namespace App\Service;

use App\Repository\UserAccountRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class LoginService
 *
 * @package App\Service
 */
class LoginService
{
    /**
     * 注入的參數
     *
     * @var UserAccountRepository
     */
    protected $UserAccountRepository;

    /**
     * LoginService constructor
     *
     * @param UserAccountRepository $userAccountRepository
     */
    public function __construct(UserAccountRepository $userAccountRepository) {
        $this->UserAccountRepository = $userAccountRepository;
    }

    /**
     *  判斷帳號是否存在
     *
     * @param  string $account  帳號
     * @param  string $password 密碼
     * @return boolean true=有此帳號, false=無此帳號
     */
    public function isUser($account, $password) {
        /** @var \Illuminate\Database\Eloquent\Model 對應帳號的用戶資訊 */
        $user = $this->UserAccountRepository->getUser($account);
        if (count($user) == 1) {
            if (password_verify($password, $user->Password)) {
                Auth::login($user);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
