<?php
/**
 * 主頁的Service
 *
 * @author 重甫
 * @since 06/25/19
 */
namespace App\Service;

use App\Repository\UserWebsiteContentRepository;
use App\Repository\UserWebsiteContentMemberRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class MainService
 *
 * @package App\Service
 */
class MainService
{
    /**
     * 注入的參數
     *
     * @var UserWebsiteContentRepository
     * @var UserWebsiteContentMemberRepository
     */

    protected $UserWebsiteContentRepository;
    protected $UserWebsiteContentMemberRepository;

    /**
     * MainService constructor
     *
     * @param UserWebsiteContentRepository $userWebsiteContentRepository
     * @param UserWebsiteContentMemberRepository $userWebsiteContentMemberRepository
     */
    public function __construct(UserWebsiteContentRepository $userWebsiteContentRepository,
                                UserWebsiteContentMemberRepository $userWebsiteContentMemberRepository) {
        $this->UserWebsiteContentRepository = $userWebsiteContentRepository;
        $this->UserWebsiteContentMemberRepository = $userWebsiteContentMemberRepository;
    }

    /**
     * 設定儲存內容
     *
     * @param array $website_content 網站內容
     * @return int $user_website_content_id 網站成員內容ID
     */
    public function setSaveContent($website_content) {
        /** @var string 網站內容ID */
        $user_website_content_id = "";

        foreach ($website_content as $_website_content) {
            foreach ($_website_content as $__website_content) {
                /** 判斷是否是空的 */
                if ($__website_content["user_website_id"] == null || $__website_content["condition_x"] == null || $__website_content["condition_y"] == null) {
                    break;
                } else {
                    /** @var \Illuminate\Database\Eloquent\Model 新增或更新使用者儲存的網站內容 */
                    $user_website_content = $this->UserWebsiteContentRepository->setUserWebsiteContent($__website_content);
                    /** @var int  網站內容ID*/
                    $user_website_content_id = $this->UserWebsiteContentRepository->getUserWebsiteContentId($__website_content);
                }
                /** 刪除網站成員內容 */
                $this->UserWebsiteContentMemberRepository->deleteMemberContent($user_website_content["UserWebsiteContentId"]);

                foreach ($__website_content["user_website_content_member"] as $user_website_content_member) {
                    /** @var int 成員 */
                    $member_from = is_null($user_website_content_member["MemberFrom"]) ? 0 : $user_website_content_member["MemberFrom"];
                    /** @var string %數 */
                    $percent_from = is_null($user_website_content_member["PercentFrom"]) ? "" : $user_website_content_member["PercentFrom"];
                    /** @var int 成員 */
                    $member_to = is_null($user_website_content_member["MemberTo"]) ? 0 : $user_website_content_member["MemberTo"];
                    /** @var string %數 */
                    $percent_to = is_null($user_website_content_member["PercentTo"]) ? "" : $user_website_content_member["PercentTo"];

                    /** 新增使用者儲存的網站成員內容 */
                    $this->UserWebsiteContentMemberRepository->newMemberContent($user_website_content["UserWebsiteContentId"], $member_from, $percent_from, $member_to, $percent_to);
                }
            }
        }
        return $user_website_content_id;
    }
}
