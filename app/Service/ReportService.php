<?php
/**
 * 報表的Service
 *
 * @author 重甫
 * @since 05/06/19
 */
namespace App\Service;

use App\Repository\ReportRepository;
use App\Repository\ReportMemberRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class ReportService
 *
 * @package App\Service
 */
class ReportService
{
    /**
     * 注入的參數
     *
     * @var ReportRepository
     * @var ReportMemberRepository
     */
    protected $ReportRepository;
    protected $ReportMemberRepository;

    /**
     * ReportService constructor
     *
     * @param ReportRepository $reportRepository
     * @param ReportMemberRepository $reportMemberRepository
     */
    public function __construct(ReportRepository $reportRepository,
                                ReportMemberRepository $reportMemberRepository) {
        $this->ReportRepository = $reportRepository;
        $this->ReportMemberRepository = $reportMemberRepository;
    }

    /**
     * 設定報表
     *
     * @param  object $report_content 報表內容
     * @param  int $user_website_content_id 成員內容ID
     * @return boolean 設定資料表的結果
     */
    public function setReport($report_content, $user_website_content_id) {
        foreach ($report_content["money"] as $_report_content) {
            /** @var int 成員1,如果是空給預設 */
            $member_from = is_null($_report_content[0]) ? 0 : $_report_content[0];
            /** @var int 成員1的錢,如果是空給預設 */
            $money_from  = is_null($_report_content[1]) ? 0 : $_report_content[1];
            /** @var int 成員2,如果是空給預設 */
            $member_to   = is_null($_report_content[2]) ? 0 : $_report_content[2];
            /** @var int 成員2的錢,如果是空給預設 */
            $money_to    = is_null($_report_content[3]) ? 0 : $_report_content[3];
            /** @var int 報表成員ID */
            $report_member_id = $this->ReportRepository->getReportMemberId(Auth::user()->UserAccountId, $report_content["website"], $report_content["account"], $report_content["name"], $report_content["group"], $report_content["date"], $member_from, $member_to);

            if (is_null($report_member_id)) {
                 /** @var int 新增報表成員及前並取得ID */
                $report_member_id = $this->ReportMemberRepository->newOneGetId($member_from, $money_from, $member_to, $money_to);
                // /** @var boolean 新增報表主要內容 */
                $result = $this->ReportRepository->newOne(Auth::user()->UserAccountId, $report_content["website"], $report_content["account"], $report_content["name"], $report_content["group"], $report_member_id, $user_website_content_id, $report_content["date"]);
            } else {
                $result = $this->ReportMemberRepository->setMemberMoney($money_from, $money_to, $report_member_id);
            }
        }

        return (boolean)$result;
    }

    /**
     * 取得符合條件的報表網站
     *
     * @param  string $kind 種類
     * @param  string $Date 日期
     * @return \Illuminate\Database\Eloquent\Collection 符合條件的報表網站內容
     */
    public function getWebsite($kind, $Date) {
        /** @var \Illuminate\Database\Eloquent\Builder 有報表的網站 */
        $report_builder = $this->ReportRepository->getReportWebsite();

        switch ($kind) {
            case "group_date":
                /** @var \Illuminate\Database\Eloquent\Collection 把相同的日期群組起來 */
                return $report_date_group = $report_builder->groupby("Date")->pluck("Date");
            default:
                /** @var \Illuminate\Database\Eloquent\Collection 有報表的網站日期篩選 */
                return $report_website = $report_builder->where('Date', '=', $Date)->get();
        }
    }

    /**
     * 判斷有沒有符合的值
     *
     * @param  string $kind  種類
     * @param  string $value 要判斷的值
     * @return string 符合的值
     */
    public function isValue($kind, $value) {
        if ($value == "") {
            switch ($kind) {
                /** 日期 */
                case "date":
                    /** @var \Illuminate\Database\Eloquent\Collection 日期 */
                    $value = $this->ReportRepository->getReportWebsite()->groupby("Date")->pluck("Date");
                    if ($value->isEmpty()) {
                        $value = "";
                    } else {
                        $value = $value[0];
                    }
                    break;
            }
        }
        return $value;
    }
}
