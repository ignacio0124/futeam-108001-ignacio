<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserAccount', function (Blueprint $table) {
            $table->increments('UserAccountId');
            $table->text('Account');
            $table->text('Password');
            $table->text('Name');
            $table->enum('Permission', ['Administrator', 'Member', 'Customer']);
            $table->timestamp('AddTime')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserAccount');
    }
}
