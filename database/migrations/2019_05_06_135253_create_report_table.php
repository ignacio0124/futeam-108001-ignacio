<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Report', function (Blueprint $table) {
            $table->increments('ReportId');
            $table->unsignedInteger('UserAccountId')->index();
            $table->unsignedInteger('WebsiteId')->index();
            $table->text('Account');
            $table->unsignedInteger('ReportMemberId')->index();
            $table->text('Time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Report');
    }
}
