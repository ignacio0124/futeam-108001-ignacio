<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ReportMember', function (Blueprint $table) {
            $table->increments('ReportMemberId');
            $table->unsignedInteger('MemberFrom')->index();
            $table->Integer('MoneyFrom');
            $table->unsignedInteger('MemberTo')->index();
            $table->unsignedInteger('MoneyTo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ReportMember');
    }
}
