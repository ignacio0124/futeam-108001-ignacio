<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ReportMember', function (Blueprint $table) {
            $table->Integer('MoneyFrom')->change();
            $table->Integer('MoneyTo')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ReportMember', function (Blueprint $table) {
            $table->text('MoneyFrom')->change();
            $table->text('MoneyTo')->change();
        });
    }
}
