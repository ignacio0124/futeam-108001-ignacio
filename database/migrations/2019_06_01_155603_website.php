<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Website extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Website', function (Blueprint $table) {
            DB::statement("ALTER TABLE Website MODIFY COLUMN Kind ENUM('數', '運', '現') NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Website', function (Blueprint $table) {
            DB::statement("ALTER TABLE Website MODIFY COLUMN Kind ENUM('六', '運', '現') NOT NULL");
        });
    }
}
