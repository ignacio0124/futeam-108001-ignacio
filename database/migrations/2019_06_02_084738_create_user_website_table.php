<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWebsiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserWebsite', function (Blueprint $table) {
            $table->increments('UserWebsiteId');
            $table->unsignedInteger('UserAccountId')->index();
            $table->unsignedInteger('WebsiteId')->index();
            $table->text('Account');
            $table->text('Password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserWebsite');
    }
}
