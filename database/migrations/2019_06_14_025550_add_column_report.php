<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Report', function (Blueprint $table) {
            $table->unsignedInteger('Group')->after('Account');
            $table->text('Name')->after('Account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Report', function (Blueprint $table) {
            $table->dropColumn(['Group', 'Name']);
        });
    }
}
