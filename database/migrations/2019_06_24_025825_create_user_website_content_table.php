<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWebsiteContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserWebsiteContent', function (Blueprint $table) {
            $table->increments('UserWebsiteContentId');
            $table->unsignedInteger('UserWebsiteId')->index();
            $table->text("ConditionX");
            $table->text("ConditionY");
            $table->unsignedInteger('Group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserWebsiteContent');
    }
}
