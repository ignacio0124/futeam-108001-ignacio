<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWebsiteContentMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserWebsiteContentMember', function (Blueprint $table) {
            $table->increments('UserWebsiteContentMemberId');
            $table->unsignedInteger('UserWebsiteContentId')->index();
            $table->unsignedInteger('MemberFrom')->index();
            $table->text('PercentFrom');
            $table->unsignedInteger('MemberTo')->index();
            $table->text('PercentTo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserWebsiteContentMember');
    }
}
