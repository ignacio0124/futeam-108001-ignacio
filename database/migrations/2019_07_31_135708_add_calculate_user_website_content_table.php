<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalculateUserWebsiteContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('UserWebsiteContent', function (Blueprint $table) {
            $table->text('Plus');
            $table->unsignedInteger('PlusPriority')->nullable($value = true);
            $table->text('Minus');
            $table->unsignedInteger('MinusPriority')->nullable($value = true);
            $table->text('Times');
            $table->unsignedInteger('TimesPriority')->nullable($value = true);
            $table->text('Divided');
            $table->unsignedInteger('DividedPriority')->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('UserWebsiteContent', function (Blueprint $table) {
            $table->dropColumn(['Plus', 'Minus', 'Times', 'Divided',
                                'PlusPriority', 'MinusPriority', 'TimesPriority', 'DividedPriority']);
        });
    }
}
