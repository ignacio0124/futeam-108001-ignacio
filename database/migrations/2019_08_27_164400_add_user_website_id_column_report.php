<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserWebsiteIdColumnReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Report', function (Blueprint $table) {
            $table->unsignedInteger('UserWebsiteContentId')->after('ReportMemberId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Report', function (Blueprint $table) {
            $table->dropColumn('UserWebsiteContentId');
        });
    }
}
