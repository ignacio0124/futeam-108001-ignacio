<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class WebsiteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Website')->insert([
            'WebsiteId' => 1,
            'Kind' => '數',
            'Name' => '風雲管理',
            'Url' => 'pos.di178.net'
        ]);

        DB::table('Website')->insert([
            'WebsiteId' => 2,
            'Kind' => '數',
            'Name' => '風雲管理',
            'Url' => 'mm.di178.net'
        ]);

        DB::table('Website')->insert([
            'WebsiteId' => 3,
            'Kind' => '數',
            'Name' => '風雲管理',
            'Url' => 'ca9-dkn.cm589.net'
        ]);

        DB::table('Website')->insert([
            'WebsiteId' => 11,
            'Kind' => '數',
            'Name' => '叱吒管理',
            'Url' => 'pos.b558i.com'
        ]);

        DB::table('Website')->insert([
            'WebsiteId' => 12,
            'Kind' => '數',
            'Name' => '叱吒管理',
            'Url' => 'mail.b558i.com'
        ]);

        DB::table('Website')->insert([
            'WebsiteId' => 21,
            'Kind' => '數',
            'Name' => '彩球管理',
            'Url' => 'www.df368.net'
        ]);

        DB::table('Website')->insert([
            'WebsiteId' => 22,
            'Kind' => '數',
            'Name' => '彩球管理',
            'Url' => 'mail.df368.net'
        ]);
    }
}
