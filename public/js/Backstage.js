/**
 * @author 重甫
 * @since 05/22/19
 */

/** 必要的載入 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/** @global {string} 網址 */
HOST = window.location.origin;

/**
 * 新增成員
 */
function newMember() {
	/** @type {string} 人名 */
	var name = $("#name").val();
	/** @type {string} 帳號 */
	var account = $("#account").val();
	/** @type {int} 流水號 */
	var serial_number = $(".info").length;

	if (name == "") {
		alert("請輸入人名");
	} else {
		$.ajax({
			url:HOST + '/newMember',
			type:'POST',
			dataType:'text',
			data:{'name': name, 'account': account},
			error: function(msg) {
				console.log('error');
			},
			success: function(msg) {
				$("#name").val("");
				$("#account").val("");

				$("#add_new").before('\
					<div id="' + msg + '" class="info">\
		                <a class="info__delete" onclick="deleteMember(' + msg + ')">\
		                    <svg viewBox="0 0 10 10">\
		                        <path d="M3 3 L7 7"></path>\
		                        <path d="M7 3 L3 7"></path>\
		                    </svg>\
		                </a>\
		                <div class="info__item">#' + serial_number + '</div>\
		                <div class="info__item">\
		                    <span>姓名</span>\
		                    <input type="text" value="' + name + '" />\
		                </div>\
		                <div class="info__item">\
		                    <span>帳號</span>\
		                    <input type="text" value="' + account + '" />\
		                </div>\
		                <div class="info__item"><a class="info__item-btn">編輯</a></div>\
		            </div>\
				');
				alertify.success("新增成功");
			}
		});
	}
}

/**
 * 刪除成員
 *
 * @param {int} member_id MemberId
 */
function deleteMember(member_id) {
	confirm("確定要刪除此成員嗎？", function(isConfirm) {
		if (isConfirm) {
			$.ajax({
				url:HOST + '/deleteMember',
				type:'POST',
				dataType:'text',
				data:{'member_id': member_id},
				error: function(msg) {
					console.log('error');
				},
				success: function(msg) {
					alertify.success("刪除成功");
					window.location.reload();
				}
			});
		}
	});
}

/**
 * 編輯成員
 *
 * @param {int} member_id MemberId
 */
function setMember(member_id) {
	if ($("#"+member_id).hasClass("info_edit")) {
		/** @type {string} 人名 */
		var name = $("#"+member_id+" input").eq(0).val();
		/** @type {string} 帳號 */
		var account = $("#"+member_id+" input").eq(1).val();

		$.ajax({
			url:HOST + '/setMember',
			type:'POST',
			dataType:'text',
			data:{'member_id': member_id, 'name': name, 'account': account},
			error: function(msg) {
				console.log('error');
			},
			success: function(msg) {
				alertify.success("編輯成功");
			}
		});
	}
}
