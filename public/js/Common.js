/**
 * @author 重甫
 * @since 05/14/19
 */

/** 必要的載入 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/** @global {string} 網址 */
HOST = window.location.origin;

/**
 * 換頁
 *
 * @param {string} self 自己的頁面
 * @param {string} kind 種類
 */
function changePage(self, kind) {
	/** 判斷是不是主頁 */
	if (self == "Main") {
		/** 移除離開頁面提示訊息 */
		window.onbeforeunload = null;
		/** 呼叫Main.js的function, 儲存頁面資訊 */
		setSaveInformation();
	}

	switch (kind) {
		case "Main":
			location.href = HOST + "/Main";
			break;
		case "Report":
			location.href = HOST + "/Report";
			break;
		case "Backstage":
			location.href = HOST + "/Backstage/Member";
			break;
	}
}