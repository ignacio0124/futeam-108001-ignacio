/**
 * @author 重甫
 */

/** 必要的載入 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/** @global 網址 */
HOST = window.location.origin;

/**
 * 按下Enter登入
 *
 * @param {object} event 事件
 */
function enterLogin(event) {
	if (event.keyCode == 13) {
		Login();
	}
}

/**
 * 登入判斷
 */
function Login() {
	/** @type {string} 帳號 **/
	var account = $("#Account").val();
	/** @type {string} 密碼 **/
	var password = $("#Password").val();

	$.ajax({
		url:'Login',
		type:'POST',
		data:{'account': account, 'password': password},
		error: function (msg) {
			console.log('error');
		},
		success: function(msg) {
			if (msg == "true") {
				location.href = HOST + '/Backstage/Member';
			} else {
				if (account == "") {
					alertify.error("請輸入帳號");
				} else if (password == "") {
					alertify.error("請輸入密碼");
				} else {
					alert("輸入有誤，請重新輸入");
					$("#Account").val('');
					$("#Password").val('');
				}
			}
		}
	});
}