/**
 * @author 重甫
 * @since 03/27/19
 */

/** 必要的載入 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/** @global {string} 網址 */
HOST = window.location.origin;
/** @global {object} 報表的資訊, function setReport設定 */
REPORT = {};
/** @global {array} 記錄成員名單 function getCrawlerColumn設定 */
MEMBER = [];

/**
 * 點擊關閉下拉式選單
 */
$(function() {
	$(window).on('click', function(event) {
		/** 選擇姓名 */
		if (event.target.localName != "input" && event.target.localName != "svg" && event.target.localName != "path") {
			$("*.match_visible").removeClass("match_visible");
		}
		/** 選擇欄位及左上方選單 */
		if (event.target.className != "dropDown__select" && event.target.localName != "span" && event.target.localName != "svg") {
			$(".dropDown__menu").css("display", "none");
		}
	});
})

/**
 * 取得爬蟲抓取的欄位值
 *
 * @param {number} row 第幾列
 * @param {array} member 成員名單
 */
function getCrawlerColumn(row, member) {
	/** @type {number} 成員數量 */
	count_member = member.length;

	for (var i = 0; i < count_member; i ++) {
		/** 寫入全域變數Member紀錄 */
		MEMBER[i] = {"name": member[i]["Name"], "member_id": member[i]["MemberId"]};
	}

	/** @var {string} 去除Https及.的網址 */
	// if ($("#website_url_" + row).val().indexOf("https://") != -1) {
		var url = $("#website_url_" + row).val().replace("https://", "").replace(/\./g, "");
	// } else {
	// 	var url = $("#website_url_" + row).val().replace("http://", "").replace(/\./g, "");
	// }
	/** @var {string} 帳號 */
	var account = $("#account_" + row).val();
	/** @var {string} 密碼 */
	var password = $("#password_" + row).val();
	/** @var {string} 起始日期 */
	var date1 = $("#date_start_" + row).val();
	/** @var {string} 結束日期 */
	var date2 = $("#date_end_" + row).val();

	if (url == "") {
		alertify.error("請輸入網址");
	} else if (account == "") {
		alertify.error("請輸入帳號");
	} else if (password == "") {
		alertify.error("請輸入密碼");
	} else {
		/** 顯示讀取中的DIV */
		$(".loader").css("display", "block");

		$.ajax({
			url:HOST + '/getCrawlerColumn',
			type:'POST',
			dataType:'text',
			data:{'url': url, 'account': account, 'password': password, 'date1': date1, 'date2': date2},
			error: function(msg) {
				console.log('error');
			},
			success: function(msg) {
				/** 關閉讀取中的DIV */
				$(".loader").css("display", "none");

				if (msg == "") {
					alert("無法抓取，請聯絡管理員，謝謝");
					return;
				}
				/** @type {array} 爬蟲取得的資料 */
				var crawler_column = JSON.parse(msg);

				if (crawler_column == "" || crawler_column[0] == "") {
					alert("無法抓取，請聯絡管理員，謝謝");
				} else if (crawler_column[0].error == "website_error") {
					alert("網址輸入有誤，請重新輸入");
				} else if (crawler_column[0].error == "account_error") {
					alert("帳號或密碼輸入有誤，請重新輸入");
				} else if (crawler_column[0].error == "no_report") {
					alert("沒有資料可以抓取，請更換帳號或日期");
				} else {
					/** @type {number} 網站ID*/
					var website_id = $("#website_url_" + row).attr("website_id");
					/** @type {array} 爬蟲取得的資料 */
					var crawler_column = JSON.parse(msg);
					/** @type {array} key值 */
					var crawler_column_keys = Object.keys(crawler_column[0]);
					/** @global {array} 爬蟲取得的資料 */
					CRAWLER_COLUMN = crawler_column;

					for (var i = 0; i < crawler_column_keys.length; i ++) {
						$("div[name='crawler_dropdown_x_" + row + "']").each(function() {
							$(this).find("div").eq(1).append(
								'<div class="dropDown__li" onclick="setDropDownValue(\'' + crawler_column_keys[i] + '\', this)">' + crawler_column_keys[i] + '</div>'
							);
						});
					}

					for (var i = 0; i < crawler_column.length; i ++) {
						$("div[name='crawler_dropdown_y_" + row + "']").each(function() {
							$(this).find("div").eq(1).append(
								'<div class="dropDown__li" onclick="setDropDownValue(\'' + crawler_column[i]["客戶"] + '\', this)">' + crawler_column[i]["客戶"] + '</div>'
							);
						});
					}

					$(".table__body-container").eq(row-1).find(".triangle").addClass("triangle_up");
					$("#body_default_" + row).css("display", "block");

					alertify.success("抓取成功");
					setUserWebsite(website_id, account, password, row);
				}
			}
		});
	}
}

/**
 * 設定使用者的網站
 *
 * @param {number} website_id 網站的id
 * @param {string} account    帳號
 * @param {string} password   密碼
 * @param {number} row 		  第幾列
 */
function setUserWebsite(website_id, account, password, row) {
	$.ajax({
		url:HOST + '/setUserWebsite',
		type:'POST',
		dataType:'text',
		data:{'website_id': website_id, 'account': account, 'password': password},
		error: function(msg) {
			console.log('error');
		},
		success: function(msg) {
			/** 增加id */
			$("#website_" + row).parents(".table__body-row").attr("id", msg);
		}
	});
}

/**
 * 設定名字
 *
 * @param {element} _this 當前的元素點
 * @param {string} name 名字
 * @param {number} id 名字的id
 */
function setName(_this, name, id) {
	$(_this).closest("div[class='match match_visible']").prev("input").val(name);
	$(_this).closest("div[class='match match_visible']").prev("input").attr("member_id", id);
	$(_this).parent("ul").find("li").css("display", "flex");
}

/**
 * 設定人名下拉式選單
 *
 * @param {element} _this 當前的元素點
 */
function setNameDropDown(_this) {
	$(_this).next("div").addClass("match_visible");
}

/**
 * 設定人名下拉選單內容顯示隱藏
 *
 * @param {element} _this 當前的元素點
 */
function setNameDropDownContent(_this) {
	/** @type {string} 輸入的人名 */
	var value = $(_this).val();

	$(_this).next("div").find("li").each(function() {
		if ($(this).find("span").text().indexOf(value) == -1) {
			$(this).css("display", "none");
		} else if ($(this).find("span").text().indexOf(value) != -1) {
			$(this).css("display", "flex");
		} else {
			$(this).css("display", "flex");
		}
	});
}

/**
 * 設定下拉選單的顯示值
 *
 * @param {string} value 選擇的值
 * @param {element} _this 當前的元素點
 */
function setDropDownValue(value, _this) {
	$(_this).closest('div[class="dropDown"]').find('span').text(value);
}

/**
 * 設定日期
 *
 * @param {string} kind 選擇的日期區間
 * @param {number} row 第幾列
 */
function setDate(kind, row) {
	/** @type {timestamp} 今天的日期時間戳 */
	var today_timestamp = new Date().getTime();
	/** @type {date} 今天的日 */
	var today_day = new Date().getDay();
	/** @type {date} 今天的日期 */
	var today = new Date();

	/**
	 * 判斷本週上週起始及結束日期
	 *
	 * @param {date} this_week_start 本週起始日
	 * @param {date} this_week_end 	 本週結束日
	 * @param {date} last_week_start 上週起始日
	 * @param {date} last_week_end   上週結束日
	 */
	switch (today_day) {
		case 1:
			var this_week_start = new Date(today_timestamp);
			var this_week_end   = new Date(today_timestamp + 6 * 24 * 3600 * 1000);
			var last_week_start = new Date(today_timestamp - 7 * 24 * 3600 * 1000);
			var last_week_end   = new Date(today_timestamp - 1 * 24 * 3600 * 1000);
			break;
		case 2:
			var this_week_start = new Date(today_timestamp - 1 * 24 * 3600 * 1000);
			var this_week_end   = new Date(today_timestamp + 5 * 24 * 3600 * 1000);
			var last_week_start = new Date(today_timestamp - 8 * 24 * 3600 * 1000);
			var last_week_end   = new Date(today_timestamp - 2 * 24 * 3600 * 1000);
			break;
		case 3:
			var this_week_start = new Date(today_timestamp - 2 * 24 * 3600 * 1000);
			var this_week_end   = new Date(today_timestamp + 4 * 24 * 3600 * 1000);
			var last_week_start = new Date(today_timestamp - 9 * 24 * 3600 * 1000);
			var last_week_end   = new Date(today_timestamp - 3 * 24 * 3600 * 1000);
			break;
		case 4:
			var this_week_start = new Date(today_timestamp - 3 * 24 * 3600 * 1000);
			var this_week_end   = new Date(today_timestamp + 3 * 24 * 3600 * 1000);
			var last_week_start = new Date(today_timestamp - 10 * 24 * 3600 * 1000);
			var last_week_end   = new Date(today_timestamp - 4 * 24 * 3600 * 1000);
			break;
		case 5:
			var this_week_start = new Date(today_timestamp - 4 * 24 * 3600 * 1000);
			var this_week_end   = new Date(today_timestamp + 2 * 24 * 3600 * 1000);
			var last_week_start = new Date(today_timestamp - 11 * 24 * 3600 * 1000);
			var last_week_end   = new Date(today_timestamp - 5 * 24 * 3600 * 1000);
			break;
		case 6:
			var this_week_start = new Date(today_timestamp - 5 * 24 * 3600 * 1000);
			var this_week_end   = new Date(today_timestamp + 1 * 24 * 3600 * 1000);
			var last_week_start = new Date(today_timestamp - 12 * 24 * 3600 * 1000);
			var last_week_end   = new Date(today_timestamp - 6 * 24 * 3600 * 1000);
			break;
		case 0:
			var this_week_start = new Date(today_timestamp - 6 * 24 * 3600 * 1000);
			var this_week_end   = new Date(today_timestamp);
			var last_week_start = new Date(today_timestamp - 13 * 24 * 3600 * 1000);
			var last_week_end   = new Date(today_timestamp - 7 * 24 * 3600 * 1000);
			break;
		default:
			var this_week_start = new Date();
			var this_week_end   = new Date();
			var last_week_start = new Date();
			var last_week_end   = new Date();
			break;
	}

	/**
	 * 判斷選擇本週或上週並且轉換日期格式
	 *
	 * @param {string} date1 起始日期
	 * @param {string} date2 結束日期
	 */
	switch (kind) {
		case "this_week":
			var date1 = this_week_start.getFullYear() + '-' + (this_week_start.getMonth() + 1 < 10 ? '0' : '') + (this_week_start.getMonth() + 1) + '-' + (this_week_start.getDate() < 10 ? '0' : '') + this_week_start.getDate();
			var date2 = today.getFullYear() + '-' + (today.getMonth() + 1 < 10 ? '0' : '') + (today.getMonth() + 1) + '-' + (today.getDate() < 10 ? '0' : '') + today.getDate();
			break;
		case "last_week":
			var date1 = last_week_start.getFullYear() + '-' + (last_week_start.getMonth() + 1 < 10 ? '0' : '') + (last_week_start.getMonth() + 1) + '-' + (last_week_start.getDate() < 10 ? '0' : '') + last_week_start.getDate();
			var date2 = last_week_end.getFullYear() + '-' + (last_week_end.getMonth() + 1 < 10 ? '0' : '') + (last_week_end.getMonth() + 1) + '-' + (last_week_end.getDate() < 10 ? '0' : '') + last_week_end.getDate();
			break;
		default:
			var date1 = this_week_start.getFullYear() + '-' + (this_week_start.getMonth() + 1 < 10 ? '0' : '') + (this_week_start.getMonth() + 1) + '-' + (this_week_start.getDate() < 10 ? '0' : '') + this_week_start.getDate();
			var date2 = today.getFullYear() + '-' + (today.getMonth() + 1 < 10 ? '0' : '') + (today.getMonth() + 1) + '-' + (today.getDate() < 10 ? '0' : '') + today.getDate();
			break;
	}

	$("#date_start_" + row).val(date1);
	$("#date_end_" + row).val(date2);
	$("#date_start_" + row).attr("title", date1);
	$("#date_end_" + row).attr("title", date2);
}

/**
 * 設定網站
 *
 * @param {element} _this 當前的元素點
 * @param {number} id 網站的id
 * @param {number} row 第幾列
 */
function setWebsite(_this, id, row) {
	/** @type {string} 網站的種類 */
	var website_kind = $(_this).find("div").eq(0).text();
	/** @type {string} 網站的名稱 */
	var website_name = $(_this).find("div").eq(1).text();
	/** @type {string} 網站的網址 */
	var website_url  = $(_this).find("span").text();

	$("#website_" + row).addClass("table__body-content_match");
	$("#website_kind_" + row).text(website_kind);
	$("#website_name_" + row).text(website_name);
	$("#website_name_" + row).attr("title", website_name);
	$("#website_url_" + row).val("https://" + website_url);
	$("#website_url_" + row).attr("website_id", id);
	$("#website_drop_down_" + row).removeClass("match_visible");
	$(_this).parent("ul").find("li").css("display", "flex");
}

/**
 * 設定網站下拉式選單
 *
 * @param {string} kind 種類
 * @param {number} row 第幾列
 */
function setWebsiteDropDown(kind, row) {
	switch (kind) {
		case "open":
			$("#website_drop_down_" + row).addClass("match_visible");
			/** 設定擁有網站的陣列 */
			setWebsiteArray(row);
			break;
		case "close":
			setTimeout(function() {
				$("#website_drop_down_" + row).removeClass("match_visible");
			}, 300);
			break;
	}
}

/**
 * 設定擁有網站的陣列
 *
 * @param {number} row 第幾列
 */
function setWebsiteArray(row) {
	/** @global {array} 網站 */
	WEBSITE = [];

	$("#website_drop_down_" + row + " li").each(function() {
		WEBSITE.push($(this).find("div").eq(1).text() + " http://" + $(this).find("span").text());
	});
}

/**
 * 設定下拉選單內容顯示隱藏
 *
 * @param {element} _this 當前的元素點
 * @param {number} row 第幾列
 */
function setWebsiteDropDownContent(_this, row) {
	/** @type {string} 輸入的網址 */
	var value = $(_this).val();

	for (var i = 0; i < WEBSITE.length; i ++) {
		if (WEBSITE[i].indexOf(value) == -1) {
			$("#website_drop_down_" + row + " li").eq(i).css("display", "none");
		} else if (WEBSITE[i].indexOf(value) != -1) {
			$("#website_drop_down_" + row + " li").eq(i).css("display", "flex");
		} else {
			$("#website_drop_down_" + row + " li").eq(i).css("display", "flex");
		}
	}
}

/**
 * 設定報表的資訊
 *
 * @param {number} row 第幾列
 * @param {number} group 第幾群組
 */
function setReportInformation(row, group) {
	if (typeof CRAWLER_COLUMN == "undefined") {
		alert("請重新抓取資料後再送出，謝謝");
	} else {
		/** @type {string} 條件 */
		var condition_x = $("#body_default_" + row + " div[group='x_" + group + "']").closest('div[class="dropDown"]').find('span').text();
		/** @type {string} 條件 */
		var condition_y = $("#body_default_" + row + " div[group='y_" + group + "']").closest('div[class="dropDown"]').find('span').text();
		/** @type {array} 要+的欄位 */
		var plus = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").val().split(" ");
		var plus_str = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").val();
		/** @type {number} 優先順序 */
		var plus_priority = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").attr("priority");
		/** @type {array} 要-的欄位 */
		var minus = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").val().split(" ");
		var minus_str = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").val();
		/** @type {number} 優先順序 */
		var minus_priority = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").attr("priority");
		/** @type {array} 要x的欄位 */
		var times = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").val().split(" ");
		var times_str = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").val();
		/** @type {number} 優先順序 */
		var times_priority = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").attr("priority");
		/** @type {array} 要÷的欄位 */
		var divided = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").val().split(" ");
		var divided_str = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").val();
		/** @type {number} 優先順序 */
		var divided_priority = $("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").attr("priority");
		/** @type {number} 使用者擁有的網站ID */
		var user_website_id = $("#body_default_" + row).parent("div[class='table__body-row']").attr("id");

		for (var key in CRAWLER_COLUMN) {
			if (CRAWLER_COLUMN[key]["客戶"] == condition_y) {
				/** @type {int} 要計算的金額 */
				var money = Number(CRAWLER_COLUMN[key][condition_x]);
				for (var i = 0; i < 4; i ++) {
					if (plus_priority == i && plus_priority != "") {
						for (var j = 0; j < plus.length; j ++) {
							money += Number(CRAWLER_COLUMN[key][plus[j]]);
						}
					} else if (minus_priority == i && minus_priority != "") {
						for (var j = 0; j < minus.length; j ++) {
							money -= Number(CRAWLER_COLUMN[key][minus[j]]);
						}
					} else if (times_priority == i && times_priority != "") {
						for (var j = 0; j < times.length; j ++) {
							money *= Number(CRAWLER_COLUMN[key][times[j]]);
						}
					} else if (divided_priority == i && divided_priority != "") {
						for (var j = 0; j < divided.length; j ++) {
							money /= Number(CRAWLER_COLUMN[key][divided[j]]);
						}
					}
				}
				break;
			}
		}

		/** @type {array} 紀錄姓名及錢 */
		var name_money = [];
		/** @type {string} 網站id */
		var website_id = $("#website_url_" + row).attr("website_id");
		/** @type {string} 帳號 */
		var account = $("#account_" + row).val();
		/** @type {string} 起始日 */
		var date_start = $("#date_start_" + row).val();
		/** @type {string} 結束日 */
		var date_end = $("#date_end_" + row).val();
		/** @type {number} 判斷%數的左側預設值 */
		var left_percent = 0;
		/** @type {number} 判斷%數的右側預設值 */
		var right_percent = 0;
		/** @type {array} 使用者擁有的網站內容的成員 */
		var user_website_content_member = [];
		/** @type {array} 使用者擁有的網站內容 */
		var user_website_content = [[]];

		/** 加總%數 */
		$("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-row").each(function() {
			left_percent  += Number($(this).find("div[name='first'] input").eq(2).val());
			right_percent += Number($(this).find("div[name='second'] input").eq(2).val());
		});

		/** 比對左右%數是否相同 */
		if (left_percent == right_percent) {
			$("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-row").each(function(key) {
				if ($(this).find("div[name='first'] input").eq(0).val() == "" &&
					$(this).find("div[name='first'] input").eq(2).val() == "" &&
					$(this).find("div[name='second'] input").eq(0).val() == "" &&
					$(this).find("div[name='second'] input").eq(2).val() == "") {
					/** 判斷迴圈最後一次 */
					if (key == $("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-row").length - 1) {
						REPORT = {
							"website": website_id,
							"account": account,
							"date": date_start + "~" + date_end,
							"money": name_money,
							"group": group,
							"name": condition_y
						};

						user_website_content[0][0] = {"user_website_id": user_website_id,
						  	  		 				  "condition_x": condition_x,
						  	  		 				  "condition_y": condition_y,
						  	  		 				  "group": group,
						  	  		 				  "plus": plus_str,
						  	  		 				  "plus_priority": plus_priority,
						  	  		 				  "minus": minus_str,
						  	  		 				  "minus_priority": minus_priority,
						  	  		 				  "times": times_str,
						  	  		 				  "times_priority": times_priority,
						  	  		 				  "divided": divided_str,
						  	  		 				  "divided_priority": divided_priority,
						  	  		 				  "user_website_content_member": user_website_content_member};
						/** 設定儲存內容並且新增報表*/
						setSaveContent(user_website_content, "report");
					}
					return true;
				}

				name_money[key] = [$(this).find("div[name='first'] input").eq(0).attr("member_id"),
								   Math.round(Number($(this).find("div[name='first'] input").eq(2).val()) * money / 100 * -1),
								   $(this).find("div[name='second'] input").eq(0).attr("member_id"),
								   Math.round(Number($(this).find("div[name='second'] input").eq(2).val()) * money / 100)];
				user_website_content_member[key] = {"MemberFrom": $(this).find("div[name='first'] input").eq(0).attr("member_id"),
													"PercentFrom": $(this).find("div[name='first'] input").eq(2).val(),
													"MemberTo": $(this).find("div[name='second'] input").eq(0).attr("member_id"),
													"PercentTo": $(this).find("div[name='second'] input").eq(2).val()};
				/** 判斷迴圈最後一次 */
				if (key == $("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-row").length - 1) {
					REPORT = {
						"website": website_id,
						"account": account,
						"date": date_start + "~" + date_end,
						"money": name_money,
						"group": group,
						"name": condition_y
					};

					user_website_content[0][0] = {"user_website_id": user_website_id,
					  	  		 				  "condition_x": condition_x,
					  	  		 				  "condition_y": condition_y,
					  	  		 				  "group": group,
					  	  		 				  "plus": plus_str,
					  	  		 				  "plus_priority": plus_priority,
					  	  		 				  "minus": minus_str,
					  	  		 				  "minus_priority": minus_priority,
					  	  		 				  "times": times_str,
					  	  		 				  "times_priority": times_priority,
					  	  		 				  "divided": divided_str,
					  	  		 				  "divided_priority": divided_priority,
					  	  		 				  "user_website_content_member": user_website_content_member};
					/** 設定儲存內容並且新增報表 */
					setSaveContent(user_website_content, "report");
				}
			});
		} else {
			alert("左右兩邊填寫的資訊有問題，請檢查後修改");
		}
	}
}

/**
 * 設定報表的資訊(一次全部)
 *
 * @param {number} row 第幾列
 */
function setAllReportInformation(row) {
	if (typeof CRAWLER_COLUMN == "undefined") {
		alert("請重新抓取資料後再送出，謝謝");
	} else {
		$("#body_default_" + row + " .table__body-cust").each(function(key) {
			if (key == $("#body_default_" + row + " .table__body-cust").length - 1) {
				return false;
			}
			/** 設定報表的資訊 */
			setReportInformation(row, $(this).attr("name").substr(6));
		});
	}
}

/**
 * 設定要保存的資訊
 */
function setSaveInformation() {
	/** @type {array} 使用者擁有的網站內容 */
	var user_website_content = [];
	/** @type {number} 迴圈記數 */
	var count = 0;
	$(".table__body-row").each(function(key1) {
		count ++;
		/** @type {number} 使用者擁有的網站ID */
		var user_website_id = $(this).attr("id");
		/** 宣告成2維陣列 */
		user_website_content[key1] = [];
		/** 塞資訊進陣列 */
		$(this).find(".table__body-cust").each(function(key2) {
			if (typeof $(this).attr("name") != "undefined") {
				/** @type {array} 使用者擁有的網站內容的成員 */
				var user_website_content_member = [];
				/** 塞資訊進陣列 */
				$(this).find(".table__body-cust-row").each(function(key3) {
				    user_website_content_member[key3] = {"MemberFrom": $(this).find("div[name='first'] input").eq(0).attr("member_id"),
														 "PercentFrom": $(this).find("div[name='first'] input").eq(2).val(),
														 "MemberTo": $(this).find("div[name='second'] input").eq(0).attr("member_id"),
														 "PercentTo": $(this).find("div[name='second'] input").eq(2).val()};
				});
				/** 組成最終的陣列 */
				user_website_content[key1][key2] = {"user_website_id": user_website_id,
					  	  		 					"condition_x": $(this).find(".dropDown__select").eq(0).find("span").text(),
					  	  		 					"condition_y": $(this).find(".dropDown__select").eq(1).find("span").text(),
					  	  		 					"group": $(this).attr("name").substr(6),
					  	  		 					"plus": $(this).find("input[name='plus']").val(),
					  	  		 					"plus_priority": $(this).find("input[name='plus']").attr("priority"),
					  	  		 					"minus": $(this).find("input[name='minus']").val(),
					  	  		 					"minus_priority": $(this).find("input[name='minus']").attr("priority"),
					  	  		 					"times": $(this).find("input[name='times']").val(),
					  	  		 					"times_priority": $(this).find("input[name='times']").attr("priority"),
					  	  		 					"divided": $(this).find("input[name='divided']").val(),
					  	  		 					"divided_priority": $(this).find("input[name='divided']").attr("priority"),
					  	  		 					"user_website_content_member": user_website_content_member};
			}
		});

		/** 設定儲存內容 */
		if (count == $(".table__body-row").length) {
			setSaveContent(user_website_content, "save");
		}
	});
}

/**
 * 設定儲存內容
 *
 * @param {array}  website_content 網站內容
 * @param {string} kind 種類
 */
function setSaveContent(website_content, kind) {
	switch (kind) {
		case "save":
			$.ajax({
				url:HOST + '/setSaveContent',
				type:'POST',
				dataType:'text',
				data:{'website_content': website_content},
				error: function(msg) {
					console.log('error');
				},
				success: function(msg) {
					alertify.success("儲存完成");
				}
			});
			break;
		case "report":
			$.ajax({
				url:HOST + '/setSaveContent',
				type:'POST',
				dataType:'text',
				data:{'website_content': website_content},
				error: function(msg) {
					console.log('error');
				},
				success: function(msg) {
					/** 新增報表 */
					setReport(msg);
				}
			});
			break;
	}
}

/**
 * 設定報表資訊至資料庫
 *
 * @param {number} user_website_content_id 網站內容ID
 */
function setReport(user_website_content_id) {
	$.ajax({
		url:HOST + '/setReport',
		type:'POST',
		dataType:'text',
		data:{'report': REPORT, 'user_website_content_id': user_website_content_id},
		error: function(msg) {
			console.log('error');
		},
		success: function(msg) {
			if (msg == "true") {
				alertify.success("新增至報表成功");
			}
		}
	});
}

/**
 * 新增成員
 *
 * @param {element} _this 當前的元素點
 */
function newMember(_this) {
	/** @type {string} 要新增的姓名 */
	var name = $(_this).parent("a").prev("input").val();

	if (name == "") {
		alertify.error("請輸入要新增的姓名");
	} else {
		/** 呼叫Backstage Controller的function */
		$.ajax({
			url:HOST + '/newMember',
			type:'POST',
			dataType:'text',
			data:{'name': name},
			error: function(msg) {
				console.log('error');
			},
			success: function(msg) {
				/** 新增成員至全域變數Member */
				MEMBER.push({"name": name, "member_id": Number(msg)});
				/** 塞入成員的下拉選單 */
				$("ul[name='name_drop_down']").prepend('\
					<li class="match__item" onclick="setName(this, \'' + name + '\', ' + msg + ')">\
	                    <div class="match__item-content">\
	                        <span>' + name + '</span>\
	                    </div>\
	                </li>\
				');

				/** 清空欄位的值 */
				$(_this).parent("a").prev("input").val("");
			}
		});
	}
}

/**
 * 新增列
 *
 * @param {string} kind 種類
 * @param {number} row 第幾列
 * @param {number} group 第幾群組
 * @param {array} value 值
 */
function newRow(kind, row, group, value) {
	switch (kind) {
		case "name":
			/** 移除確認鈕 */
			$("#body_default_" + row + " a[name='confirm_" + group + "']").remove();

			$("#body_default_" + row + " div[name='group_" + group + "']").append('\
				<div class="table__body-cust-row">\
	                <div class="table__body-cust-item"></div>\
	                <div class="table__body-cust-item">\
	                    <div class="table__body-cust-li">\
	                        <div class="table__body-cust-content">\
	                            <!-- <input type="text" /> -->\
	                        </div>\
	                    </div>\
	                    <div name="first" class="table__body-cust-li">\
	                        <span>姓名 / %數:</span>\
	                        <div class="table__body-cust-content">\
	                            <div class="table__body-cust-input">\
	                                <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>\
	                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->\
	                                <div class="match">\
	                                	<div class="match_addItem">\
                                            <input type="text" placeholder="新增" />\
                                            <a>\
                                                <svg viewBox="0 0 10 10" onclick="newMember(this)">\
                                                    <path d="M5 2 L5 8"></path>\
                                                    <path d="M2 5 L8 5"></path>\
                                                </svg>\
                                            </a>\
                                        </div>\
	                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">\
	                                    </ul>\
	                                </div>\
	                            </div>\
	                            <div class="table__body-cust-input">\
	                                <input type="text" placeholder="輸入%數" />\
	                            </div>\
	                        </div>\
	                    </div>\
	                    <div class="table__body-cust-li">\
	                        <div class="table__body-cust-content">\
	                            <i>\
	                                <svg viewBox="0 0 10 10" class="triangle triangle_right">\
	                                    <polygon points="2,1 8,1 5,6"></polygon>\
	                                    <polygon points="2,5 8,5 5,9"></polygon>\
	                                </svg>\
	                            </i>\
	                        </div>\
	                    </div>\
	                    <div name="second" class="table__body-cust-li">\
	                        <span>姓名 / %數:</span>\
	                        <div class="table__body-cust-content">\
	                            <div class="table__body-cust-input">\
	                                <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>\
	                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->\
	                                <div class="match">\
	                                	<div class="match_addItem">\
                                            <input type="text" placeholder="新增" />\
                                            <a>\
                                                <svg viewBox="0 0 10 10" onclick="newMember(this)">\
                                                    <path d="M5 2 L5 8"></path>\
                                                    <path d="M2 5 L8 5"></path>\
                                                </svg>\
                                            </a>\
                                        </div>\
	                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">\
	                                    </ul>\
	                                </div>\
	                            </div>\
	                            <div class="table__body-cust-input">\
	                                <input type="text" placeholder="輸入%數" />\
	                            </div>\
	                        </div>\
	                    </div>\
	                    <div class="table__body-cust-del" onclick="deleteRow(this, \'name\', ' + row + ', ' + group + ')">\
                            <svg viewBox="0 0 10 10">\
                                <path d="M1 1 L9 9"></path>\
                                <path d="M9 1 L1 9"></path>\
                            </svg>\
                        </div>\
	                </div>\
	                <div class="table__body-cust-item">\
	                    <a name="confirm_' + group + '" onclick="setReportInformation(' + row + ', ' + group + ')" class="table__body-cust-btn">確認</a>\
	                </div>\
	            </div>\
			');
			/** 顯示第二列的XX */
			$("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-del").eq(1).removeAttr("style");
			$("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-del").eq(1).find("svg").css("display", "block");
			/** 移除成員的下拉式選單 */
			$("ul[name='name_drop_down'] li").remove();
			/** 塞入成員的下拉式選單 */
			for (var key in MEMBER) {
				$("ul[name='name_drop_down']").append('\
					<li class="match__item" onclick="setName(this, \'' + MEMBER[key]["name"] + '\',' + MEMBER[key]["member_id"] + ')">\
                        <div class="match__item-content">\
                            <span>' + MEMBER[key]["name"] + '</span>\
                        </div>\
                    </li>\
				');
			}
			/** 補上下拉捲軸 */
			$(".scrollbar-macosx").scrollbar();
			break;
		case "group":
			/** @type {number} 有幾個群組 */
			var count_group = $("#body_default_" + row + " .table__body-cust").length;
			/** @type {number} 最後一個群組的號碼 */
			var last_group = $("#body_default_" + row + " .table__body-cust").last().prev(".table__body-cust").attr("name").substr(6);

			$.ajax({
				url:HOST + '/getGroupDiv',
				type:'POST',
				dataType:'text',
				data:{'row': row, 'count_group': count_group, 'last_group': last_group},
				error: function(msg) {
					console.log('error');
				},
				success: function(msg) {
					$("#body_default_" + row + " .table__body-cust").last().before(msg);
					/** @type {number} 第幾群組 */
					var group = Number(last_group) + 1;
					/** 補上下拉捲軸 */
					$(".scrollbar-macosx").scrollbar();
					/** 重新綁定下拉選單展開關閉事件 */
					$("#body_default_" + row + " div[name='group_" + group + "'] .dropDown__select").each(function() {
						$(this).click(function() {
							var drop = $(".dropDown");
							var menu = $(this).next(".dropDown__menu");
							if (menu.css("display") === "none") {
								drop.find(".dropDown__menu").slideUp();
								menu.slideToggle(300);
							} else {
								drop.find(".dropDown__menu").slideUp();
							}
						});
					});

					/** 確定有先抓取過爬蟲資料才做 */
					if (typeof CRAWLER_COLUMN != "undefined") {
						/** @type {array} key值 */
						var crawler_column_keys = Object.keys(CRAWLER_COLUMN[0]);

						for (var i = 0; i < crawler_column_keys.length; i ++) {
							$("#body_default_" + row + " div[name='group_" + group + "'] div[group='x_" + group + "'] div").eq(1).append(
								'<div class="dropDown__li" onclick="setDropDownValue(\'' + crawler_column_keys[i] + '\', this)">' + crawler_column_keys[i] + '</div>'
							);
						}

						for (var i = 0; i < CRAWLER_COLUMN.length; i ++) {
							$("#body_default_" + row + " div[name='group_" + group + "'] div[group='y_" + group + "'] div").eq(1).append(
								'<div class="dropDown__li" onclick="setDropDownValue(\'' + CRAWLER_COLUMN[i]["客戶"] + '\', this)">' + CRAWLER_COLUMN[i]["客戶"] + '</div>'
							);
						}
					}
				}
			});
			break;
		case "website":
			/** @type {number} 第幾列 */
			var row = $(".table__body-row").length + 1;

			$.ajax({
				url:HOST + '/getRowDiv',
				type:'POST',
				dataType:'text',
				data:{'row': row},
				error: function(msg) {
					console.log('error');
				},
				success: function(msg) {
					$(".table__body").append(msg);
				}
			});
			break;
	}
}

/**
 * 刪除列
 *
 * @param {element} _this 當前的元素點
 * @param {string} kind  種類
 * @param {number} row   第幾列
 * @param {number} group 第幾群組
 */
function deleteRow(_this, kind, row, group) {
	switch (kind) {
		case "name":
			$(_this).parents(".table__body-cust-row").remove();
			/** @type {number} 總共列數 */
			var row_count = $("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-row").length;
			/** 移除確認按鈕 */
			$("#body_default_" + row + " a[name='confirm_" + group + "']").remove();
			/** 補上確認按鈕 */
			$("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-row").eq(row_count - 1).find(".table__body-cust-item").eq(2).append('<a name="confirm_' + group + '" class="table__body-cust-btn">確認</a>');

			if (row_count == 2) {
				/** 隱藏XX */
				$("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-del").eq(1).css("pointer-events", "none");
				$("#body_default_" + row + " div[name='group_" + group + "'] .table__body-cust-del").eq(1).find("svg").css("display", "none");
			}
			break;
		case "group":
			$(_this).parents(".table__body-cust").remove();

			/** 重新編號流水號 */
			$("#body_default_" + row + " .table__body-cust").each(function(key) {
				if (key  == $("#body_default_" + row + " .table__body-cust").length - 1) {
					return;
				}
				$(this).find("span").eq(0).text(key + 1);
			});
			break;
	}
}

/**
 * 刪除儲存的網站內容
 *
 * @param  {element} _this 當前的元素點
 * @param  {number} user_website_content_id 使用者儲存網站內容的id
 */
function deleteWebsiteContent(_this, user_website_content_id) {
	$(_this).parents(".table__body-cust").remove();

	$.ajax({
		url:HOST + '/deleteWebsiteContent',
		type:'POST',
		dataType:'text',
		data:{'user_website_content_id': user_website_content_id},
		error: function(msg) {
			console.log('error');
		},
		success: function(msg) {
		}
	});
}

/**
 * 顯示彈跳視窗(計算)
 *
 * @param {number} row   第幾列
 * @param {number} group 第幾群組
 */
function viewPopupCalculate(row, group) {
	if (typeof CRAWLER_COLUMN == "undefined") {
		var crawler_column = [];
	} else {
		var crawler_column = Object.keys(CRAWLER_COLUMN[0]);
	}

	/** @type {object} 計算的值 */
	var calculate = {
		"plus": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").val(),
		"plus_priority": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").attr("priority"),
		"minus": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").val(),
		"minus_priority": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").attr("priority"),
		"times": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").val(),
		"times_priority": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").attr("priority"),
		"divided": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").val(),
		"divided_priority": $("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").attr("priority")
	}

	$.ajax({
		url:HOST + '/Popup/Calculate',
		type:'GET',
		dataType:'text',
		data:{'row': row, 'group': group, 'crawler_column': crawler_column, 'calculate': calculate},
		error: function(msg) {
			console.log('error');
		},
		success: function(msg) {
			$("#popup").append(msg);
			$("#popup").css("display", "block");
		}
	});
}




