/**
 * @author 重甫
 * @since 07/21/19
 */

/** 必要的載入 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/** @global {string} 網址 */
HOST = window.location.origin;

/**
 * 設定計算的列表
 *
 * @param {string} kind 符號種類
 * @param {string} column 欄位值
 * @param {number} row 第幾列
 * @param {number} group 第幾群組
 */
function setCalculateRow(kind, column, row, group) {
	/** @type {number} 流水號 */
	var serial_number = $("div[class='popup__item']").length + 1;
	/** @type {object} 欄位值 */
	column = JSON.parse(column);

	/**
	 * 判斷點選的符號
	 *
	 * @param {string} symbol 符號
	 */
	switch (kind) {
		case "plus":
			var symbol = "+";
			$("#plus_btn").css("pointer-events", "none");
			break;
		case "minus":
			var symbol = "-";
			$("#minus_btn").css("pointer-events", "none");
			break;
		case "times":
			var symbol = "*";
			$("#times_btn").css("pointer-events", "none");
			break;
		case "divided":
			var symbol = "÷";
			$("#divided_btn").css("pointer-events", "none");
			break;
		default:
			var symbol = "";
			break;
	}

	$("#calculator_row").append('\
		<div name="' + kind + '" class="popup__item">\
	        <div class="popup__item-number">' + serial_number + '</div>\
	        <div class="popup__item-content">\
	            <div class="dropDown">\
	                <div class="dropDown__select" onclick="setDropDown(this)">\
	                    <span>(' + symbol + ')</span>\
	                    <i>\
	                        <svg viewBox="0 0 10 10" class="triangle">\
	                            <polygon points="2,1 8,1 5,6"></polygon>\
	                            <polygon points="2,5 8,5 5,9"></polygon>\
	                        </svg>\
	                    </i>\
	                </div>\
	                <div class="dropDown__menu">\
	                    <div class="dropDown__content scrollbar-macosx">\
	                    </div>\
	                </div>\
	            </div>\
	        </div>\
	        <a class="popup__item-btn" onclick="deleteCalculateRow(this, \'' + kind + '\', ' + row + ', ' + group + ')">\
	            <svg viewBox="0 0 10 10">\
	                <path d="M2 2 L8 8"></path>\
	                <path d="M8 2 L2 8"></path>\
	            </svg>\
	        </a>\
	    </div>\
	');

	for (var key in column) {
		$(".popup__item").last().find(".dropDown__content").append('\
			<label class="dropDown__checkbox" name="popup" for="' + kind + key + '">\
	            <input type="checkbox" onclick="setCalculateValue(\'' + column[key] + '\', this)" id="' + kind + key + '" />\
	            <div class="checkbox"></div>\
	            <span>' + column[key] + '</span>\
	        </label>\
		');
	}

	/** 補上下拉捲軸 */
	$(".scrollbar-macosx").scrollbar();
}

/**
 * 設定下拉選單
 * @param {element} _this 當前的元素點
 */
function setDropDown(_this) {
	if ($(_this).next(".dropDown__menu").find("label").length == 0) {
		alert("請返回重新抓取資料後再點開，謝謝");
	} else {
		if ($(_this).next(".dropDown__menu").css("display") == "none") {
			$(_this).next(".dropDown__menu").css("display", "block");
		} else {
			$(_this).next(".dropDown__menu").css("display", "none");
		}
	}
}

/**
 * 設定要計算的值
 *
 * @param {string} value 值
 * @param {element} _this 當前的元素點
 */
function setCalculateValue(value, _this) {
	/** @type {string} 目前顯示的值 */
	var now_value = $(_this).closest(".dropDown__menu").prev(".dropDown__select").find("span").text();

	if ($(_this).prop("checked") == true) {
		/** @type {string} 新的值 */
		var new_value = now_value + " " + value;
	} else if ($(_this).prop("checked") == false) {
		/** @type {string} 新的值 */
		var new_value = now_value.replace(" " + value, "");
	}

	$(_this).closest(".dropDown__menu").prev(".dropDown__select").find("span").text(new_value);
}

/**
 * 刪除計算的列表
 *
 * @param {element} _this 當前的元素點
 * @param {string} kind 種類
 * @param {number} row 第幾列
 * @param {number} group 第幾群組
 */
function deleteCalculateRow(_this, kind, row, group) {
	$(_this).parent("div").remove();

	$("#calculator_row .popup__item").each(function(key) {
		$(this).find(".popup__item-number").text(key + 1);
	});

	/**
	 * 判斷點選的符號
	 *
	 * @param {string} symbol 符號
	 */
	switch (kind) {
		case "plus":
			$("#plus_btn").css("pointer-events", "auto");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").val("");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").attr("priority", "");
			break;
		case "minus":
			$("#minus_btn").css("pointer-events", "auto");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").val("");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").attr("priority", "");
			break;
		case "times":
			$("#times_btn").css("pointer-events", "auto");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").val("");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").attr("priority", "");
			break;
		case "divided":
			$("#divided_btn").css("pointer-events", "auto");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").val("");
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").attr("priority", "");
			break;
	}
}

/**
 * 送出所選的計算
 *
 * @param  {number} row   第幾列
 * @param  {number} group 第幾群組
 */
function confirmPopupCalculate(row, group) {
	$("#calculator_row .popup__item").each(function(key) {
		if ($(this).attr("name") == "plus") {
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").val($(this).find("span").eq(0).text().substr(4));
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='plus']").attr("priority", key);
		} else if ($(this).attr("name") == "minus") {
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").val($(this).find("span").eq(0).text().substr(4));
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='minus']").attr("priority", key);
		} else if ($(this).attr("name") == "times") {
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").val($(this).find("span").eq(0).text().substr(4));
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='times']").attr("priority", key);
		} else if ($(this).attr("name") == "divided") {
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").val($(this).find("span").eq(0).text().substr(4));
			$("#body_default_" + row + " div[name='group_" + group + "'] input[name='divided']").attr("priority", key);
		}
	});
	alertify.success("設定成功");
	closePopupCalculate();
}

/**
 * 關閉計算的彈跳視窗
 */
function closePopupCalculate() {
	$("#popup").empty();
	$("#popup").css("display", "none");
}

