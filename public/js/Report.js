/**
 * @author 重甫
 * @since 05/13/19
 */

/** 必要的載入 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/** @global {string} 網址 */
HOST = window.location.origin;

/**
 * 顯示有日期條件的報表
 *
 * @param {string} date 日期
 */
function viewDateReport(date) {
	location.href = HOST + "/Report/" + date;
}

/**
 * 篩選報表成員的顯示
 *
 * @param {int} member_number 成員編號
 * @param {element} _this 當前的元素點
 */
function setMemberFilter(member_number, _this) {
	/** @type {boolean} 是否有勾選 */
	var checked = $(_this).prop("checked");

	switch (checked) {
		case true:
			$("[name='" + member_number + "']").css("display", "flex");
			$("#all").prop("checked", true);
			$(".dropDown__content input").each(function() {
				if ($(this).prop("checked") == false) {
					$("#all").prop("checked", false);
					return false;
				}
			});
			break;
		case false:
			$("[name='" + member_number + "']").css("display", "none");
			$("#all").prop("checked", false);
			break;
	}
}

/**
 * 篩選報表成員的顯示(全選、全不選)
 *
 * @param {element} _this 當前的元素點
 */
function setAllMemberFilter(_this) {
	/** @type {boolean} 是否有勾選 */
	var checked = $(_this).prop("checked");

	switch (checked) {
		case true:
			$("input").prop("checked", true);
			$(".board__right .board__header-item").css("display", "flex");
			$(".board__right .board__item").css("display", "flex");
			$(".board__right .board__footer-item").css("display", "flex");
			break;
		case false:
			$("input").prop("checked", false);
			$(".board__right .board__header-item").css("display", "none");
			$(".board__right .board__item").css("display", "none");
			$(".board__right .board__footer-item").css("display", "none");
			break;
	}
}