function confirm(value , callback){
    swal({
        allowOutsideClick :true,
        title: '',
        text: value,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        confirmButtonColor: "rgba(254,140,8,1)",
    }, function(isConfirm){
        callback(isConfirm)
    });
}

function alert(value){
    swal({
        allowOutsideClick :true,
        title: '',
        text: value,
        showConfirmButton: true,
        showCancelButton: false,
        confirmButtonText: "OK",
        cancelButtonText: "No",
        confirmButtonColor: "rgba(254,140,8,1)",
    });
}
