$(document).ready(function() {
  $(".scrollbar-macosx").scrollbar();

  // 開關收合欄位
  $(".table__body-content-toggle").unbind();
  $(".table__body-content-toggle").click(function() {
    var parent = $(this).parents(".table__body-container");
    parent.next(".table__body-cust-wrap").slideToggle(300);
    $(this)
      .find(".triangle")
      .toggleClass("triangle_up");
  });

  // 下拉選單
  $(".dropDown__select").unbind();
  $(".dropDown__select").each(function() {
    $(this).click(function() {
      var drop = $(".dropDown");
      var menu = $(this).next(".dropDown__menu");
      if (menu.css("display") === "none") {
        drop.find(".dropDown__menu").slideUp();
        menu.slideToggle(300);
      } else {
        drop.find(".dropDown__menu").slideUp();
      }
    });
  });

  // 表格滑動
  $(".board__right .board__body").scrollbar({
    onScroll: function(y, x) {
      // console.log(y.scroll);
      let leftBody = $(".board__left .slideBar");
      let rightHead = $(".board__right .board__header .slideBar");
      let rightFooter = $(".board__right .board__footer .slideBar");
      leftBody.css({
        transform: "translateY(" + -y.scroll + "px)"
      });
      rightHead.css({
        transform: "translateX(" + -x.scroll + "px)"
      });
      rightFooter.css({
        transform: "translateX(" + -x.scroll + "px)"
      });
    }
  });

  // 切換修改帳號狀態
  $(".info").each(function() {
    const self = $(this);
    const toggleBtn = self.find(".info__item-btn");
    function check() {}
    // 在非編輯狀態下加入readonly
    if (self.hasClass("info_edit") && !self.hasClass("info_addNew")) {
      self.find(".info__item-btn").text("確認");
      self.find("input").attr("readonly", false);
    } else if (self.hasClass("info_addNew")) {
      self.removeClass("info_edit");
    } else {
      self.find(".info__item-btn").text("編輯");
      self.find("input").attr("readonly", true);
    }
    toggleBtn.click(function() {
      if (self.hasClass("info_edit") && !self.hasClass("info_addNew")) {
        self.removeClass("info_edit");
        self.find(".info__item-btn").text("編輯");
        self.find("input").attr("readonly", true);
      } else if (self.hasClass("info_addNew")) {
        self.removeClass("info_edit");
        // self.find(".info__item-btn").text("確認");
      } else {
        self.addClass("info_edit");
        self.find(".info__item-btn").text("確認");
        self.find("input").attr("readonly", false);
      }
    });
  });
});
