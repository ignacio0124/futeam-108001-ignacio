<!DOCTYPE html>
<html lang="en">
    <head>
        <title>後台</title>
        <meta charset="UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="{{asset('/')}}css/reset.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/jquery.scrollbar.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/alertify.min.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/default.min.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/sweetalert.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/icon.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/admin.css" />
        <script src="{{asset('/')}}js/jquery.js"></script>
        <script src="{{asset('/')}}js/jquery.scrollbar.min.js"></script>
        <script src="{{asset('/')}}js/alertify.min.js"></script>
        <script src="{{asset('/')}}js/sweetalert.js"></script>
        <script src="{{asset('/')}}js/sweetalert.ui.js"></script>
        <script src="{{asset('/')}}js/Common.js"></script>
        <script src="{{asset('/')}}js/Backstage.js"></script>
    </head>
    <body>
        <div class="wrap">
            <header class="header">
                <div class="header__states">
                    <div class="header__states-icon icn__home"></div>
                    <div class="dropDown">
                        <div class="dropDown__select">
                            <span>後台</span>
                            <i>
                                <svg viewBox="0 0 10 10" class="triangle">
                                    <polygon points="2,3 8,3 5,8"></polygon>
                                </svg>
                            </i>
                        </div>
                        <div class="dropDown__menu" style="display: none;">
                            <div class="dropDown__content scrollbar-macosx">
                                <div class="dropDown__li" onclick="changePage('Backstage', 'Main')">主頁</div>
                                <div class="dropDown__li">後台</div>
                                <div class="dropDown__li" onclick="changePage('Backstage', 'Report')">報表</div>
                                <div class="dropDown__li" onclick="window.location='{{ URL('Logout') }}'">登出</div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="header__user">{{ $account }}</span>
            </header>
            <main class="main">
                <div class="main__header">
                    <span class="main__header-title">BACKSTAGE</span>
                </div>
                <div class="main__tabbar">
                    <div class="main__tab main__tab_active">成員管理</div>
                    {{-- <div class="main__tab">人員管理2</div> --}}
                </div>
                <div class="main__body">
                    <div class="info-cotainer scrollbar-macosx">
                        <div class="info-contant">
                            @foreach ($member as $key => $_member)
                                <div id="{{ $_member->MemberId }}" class="info">
                                    <a class="info__delete" onclick="deleteMember({{ $_member->MemberId }})">
                                        <svg viewBox="0 0 10 10">
                                            <path d="M3 3 L7 7"></path>
                                            <path d="M7 3 L3 7"></path>
                                        </svg>
                                    </a>
                                    <div class="info__item">#{{ $key + 1 }}</div>
                                    <div class="info__item">
                                        <span>姓名</span>
                                        <input type="text" value="{{ $_member->Name }}" />
                                    </div>
                                    <div class="info__item">
                                        <span>帳號</span>
                                        <input type="text" value="{{ $_member->Account }}" />
                                    </div>
                                    <div class="info__item"><a class="info__item-btn" onclick="setMember({{ $_member->MemberId }})">編輯</a></div>
                                </div>
                            @endforeach
                            <div id="add_new" class="info info_addNew">
                                <a class="info__delete">
                                    <svg viewBox="0 0 10 10">
                                        <path d="M3 3 L7 7"></path>
                                        <path d="M7 3 L3 7"></path>
                                    </svg>
                                </a>
                                <div class="info__item">#</div>
                                <div class="info__item">
                                    <span>姓名</span>
                                    <input id="name" type="text" placeholder="請輸入姓名" />
                                </div>
                                <div class="info__item">
                                    <span>帳號</span>
                                    <input id="account" type="text" placeholder="請輸入帳號" />
                                </div>
                                <div class="info__item"><a class="info__item-btn" onclick="newMember()">新增</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <script src="{{asset('/')}}js/ui.js"></script>
    </body>
</html>
