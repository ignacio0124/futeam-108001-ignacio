<!doctype html>
<html lang="en">
    <head>
        <title>登入</title>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('/')}}css/Login.css">
        <link rel="stylesheet" href="{{asset('/')}}css/alertify.min.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/default.min.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/sweetalert.css" />
        <script src="{{asset('/')}}js/jquery.js"></script>
        <script src="{{asset('/')}}js/alertify.min.js"></script>
        <script src="{{asset('/')}}js/sweetalert.js"></script>
        <script src="{{asset('/')}}js/sweetalert.ui.js"></script>
        <script src="{{asset('/')}}js/Login.js"></script>
    </head>
    <body onkeyup="enterLogin(event)">
        <div class="container">
            <form class="form-signin">
                <h1 class="h3 mb-3 font-weight-normal text-center">登入</h1>
                <div class="form-group">
                    <label for="inputAccount">帳號：</label>
                    <input type="text" id="Account" class="form-control" placeholder="請輸入帳號" required autofocus>
                </div>
                <div class="form-group">
                    <label for="inputPassword">密碼：</label>
                    <input type="password" id="Password" class="form-control" placeholder="請輸入密碼" required>
                </div>
                <input type="button" value="確定" class="btn btn-lg btn-primary btn-block" style="background-color: #fd7e14; border-color: #fd7e14;" onclick="Login()">
            </form>
        </div>
    </body>
</html>