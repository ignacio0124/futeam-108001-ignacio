<!DOCTYPE html>
<html lang="en">
    <head>
        <title>主頁</title>
        <meta charset="UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="{{asset('/')}}css/reset.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/jquery.scrollbar.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/alertify.min.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/default.min.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/sweetalert.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/loader.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/icon.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/table.css" />
        <script src="{{asset('/')}}js/jquery.js"></script>
        <script src="{{asset('/')}}js/jquery.scrollbar.min.js"></script>
        <script src="{{asset('/')}}js/alertify.min.js"></script>
        <script src="{{asset('/')}}js/sweetalert.js"></script>
        <script src="{{asset('/')}}js/sweetalert.ui.js"></script>
        <script src="{{asset('/')}}js/Main.js"></script>
        <script src="{{asset('/')}}js/Common.js"></script>
    </head>
    <body>
        @inject('MainPresenter','App\Presenter\MainPresenter')
        <!-- 等待讀取的樣式 -->
        <div class='loader' style="display: none;">
            <div class='loader_overlay'></div>
            <div class='loader_cogs'>
                <div class='loader_cogs__top'>
                    <div class='top_part'></div>
                    <div class='top_part'></div>
                    <div class='top_part'></div>
                    <div class='top_hole'></div>
                </div>
                <div class='loader_cogs__left'>
                    <div class='left_part'></div>
                    <div class='left_part'></div>
                    <div class='left_part'></div>
                    <div class='left_hole'></div>
                </div>
                <div class='loader_cogs__bottom'>
                    <div class='bottom_part'></div>
                    <div class='bottom_part'></div>
                    <div class='bottom_part'></div>
                    <div class='bottom_hole'></div>
                </div>
                <p>資料抓取中</p>
            </div>
        </div>
        <div id="popup" style="display: none"></div>
        <div class="wrap">
            <header class="header">
                <div class="header__states">
                    <div class="header__states-icon icn__home"></div>
                    <div class="dropDown">
                        <div class="dropDown__select">
                            <span>主頁</span>
                            <i>
                                <svg viewBox="0 0 10 10" class="triangle">
                                    <polygon points="2,3 8,3 5,8"></polygon>
                                </svg>
                            </i>
                        </div>
                        <div class="dropDown__menu" style="display: none;">
                            <div class="dropDown__content scrollbar-macosx">
                                <div class="dropDown__li">主頁</div>
                                <div class="dropDown__li" onclick="changePage('Main', 'Backstage')">後台</div>
                                <div class="dropDown__li" onclick="changePage('Main', 'Report')">報表</div>
                                <div class="dropDown__li" onclick="window.location='{{ URL('Logout') }}'">登出</div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="header__user">{{ $account }}</span>
            </header>
            <main class="main">
                <div class="main__header">
                    <span class="main__header-title">WEBSITE SETTING</span>
                    <div class="main__header-btnContain">
                        <a class="main__header-btn" onclick="setSaveInformation()">儲存</a>
                        <a class="main__header-btn" onclick="newRow('website', '')">增加欄位</a>
                    </div>
                </div>
                <div class="main__body">
                    <div class="table">
                        <div class="table__header">
                            <div class="table__header-item">#</div>
                            <div class="table__header-item">網址</div>
                            <div class="table__header-item">帳號</div>
                            <div class="table__header-item">密碼</div>
                            <div class="table__header-item">日期區間</div>
                            <div class="table__header-item">抓取欄位</div>
                        </div>
                        <div class="table__body">
                            @foreach ($user_website as $key => $_user_website)
                                <div id="{{ $_user_website['UserWebsiteId'] }}" class="table__body-row">
                                    <div class="table__body-container">
                                        <div class="table__body-item">
                                            <div class="table__body-content">{{ $key + 1 }}</div>
                                        </div>
                                        <div class="table__body-item">
                                            <!-- 當輸入的網址有匹配的結果時加上table__body-content_match -->
                                            <div id="website_{{ $key + 1 }}" class="table__body-content table__body-content_match">
                                                <!-- table__body-content-symbol_orange,table__body-content-symbol_blue,table__body-content-symbol_green -->
                                                <div id="website_kind_{{ $key + 1 }}" class="table__body-content-symbol table__body-content-symbol_orange">{{ $_user_website->Website->Kind }}</div>
                                                <div id="website_name_{{ $key + 1 }}" class="table__body-content-title">{{ $_user_website->Website->Name }}</div>
                                                <input id="website_url_{{ $key + 1 }}" website_id="{{ $_user_website->Website->WebsiteId }}" type="text" value="https://{{ $_user_website->Website->Url }}" onkeyup="setWebsiteDropDownContent(this, {{ $key + 1 }})" onfocus="setWebsiteDropDown('open', {{ $key + 1 }})" onblur="setWebsiteDropDown('close', {{ $key + 1 }})"/>
                                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                <div id="website_drop_down_{{ $key + 1 }}" class="match">
                                                    <ul class="match__container scrollbar-macosx">
                                                        @foreach ($website as $_website)
                                                            <li title="{{ $_website['Name'] }}" class="match__item" onclick="setWebsite(this, {{ $_website['WebsiteId'] }}, {{ $key + 1 }})">
                                                                <!-- match__item-symbol_orange,match__item-symbol_blue,match__item-symbol_green -->
                                                                <div class="match__item-symbol match__item-symbol_orange">{{ $_website['Kind'] }}</div>
                                                                <div class="match__item-title">{{ $_website['Name'] }}</div>
                                                                <div class="match__item-content">
                                                                    <span>{{ $_website['Url'] }}</span>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table__body-item">
                                            <div class="table__body-content">
                                                <input id="account_{{ $key + 1 }}" type="text" value="{{ $_user_website->Account }}" placeholder="請輸入帳號" />
                                            </div>
                                        </div>
                                        <div class="table__body-item">
                                            <div class="table__body-content">
                                                <input id="password_{{ $key + 1 }}" type="text" value="{{ $_user_website->Password }}" placeholder="請輸入密碼" />
                                            </div>
                                        </div>
                                        <div class="table__body-item">
                                            <div class="table__body-content">
                                                <input title="{{ $date }}" id="date_start_{{ $key + 1 }}" class="table__body-content-date" type="date" value="{{ $date }}" />
                                                <div class="table__body-content-range">~</div>
                                                <input title="{{ $date }}" id="date_end_{{ $key + 1 }}" class="table__body-content-date" type="date" value="{{ $date }}" />
                                                <div class="table__body-content-radio">
                                                    <div>
                                                        <input onclick="setDate('last_week', {{ $key + 1 }})" id="a{{ $key + 1 }}" type="radio" name="week{{ $key + 1 }}" />
                                                        <label for="a{{ $key + 1 }}"></label>上週
                                                    </div>
                                                    <div>
                                                        <input onclick="setDate('this_week', {{ $key + 1 }})" id="b{{ $key + 1 }}" type="radio" name="week{{ $key + 1 }}" />
                                                        <label for="b{{ $key + 1 }}"></label>本週
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table__body-item">
                                            <div class="table__body-content">
                                                <a class="table__body-content-run" onclick="getCrawlerColumn({{ $key + 1 }}, {{ $member }})">抓取</a>
                                                <a class="table__body-content-toggle">
                                                    <svg viewBox="0 0 10 10" class="triangle">
                                                        <polygon points="2,1 8,1 5,6"></polygon>
                                                        <polygon points="2,5 8,5 5,9"></polygon>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="body_default_{{ $key + 1 }}" class="table__body-cust-wrap" style="display: none;">
                                        {!! $MainPresenter->getDefault(count($_user_website->UserWebsiteContent), $key + 1, $member, "out") !!}
                                        @foreach ($_user_website->UserWebsiteContent as $website_content_key => $_UserWebsiteContent)
                                            <div name="group_{{ $_UserWebsiteContent->Group }}" class="table__body-cust">
                                                @foreach ($_UserWebsiteContent->UserWebsiteContentMember as $website_content_member_key => $_UserWebsiteContentMember)
                                                    <div class="table__body-cust-row">
                                                        {!! $MainPresenter->getSerialNumber($website_content_member_key, $website_content_key + 1, $_UserWebsiteContent->UserWebsiteContentId) !!}
                                                        <div class="table__body-cust-item">
                                                            {!! $MainPresenter->getCondition($website_content_member_key, $_UserWebsiteContent, $key + 1) !!}
                                                            <div name="first" class="table__body-cust-li">
                                                                <span>姓名 / %數:</span>
                                                                <div class="table__body-cust-content">
                                                                    <div class="table__body-cust-input">
                                                                        <input type="text" placeholder="選擇姓名" member_id="{{ $_UserWebsiteContentMember->MemberFrom }}" value="{{ $_UserWebsiteContentMember->MemberFromMember->Name }}" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                                                        <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                                        <div class="match">
                                                                            <div class="match_addItem">
                                                                                <input type="text" placeholder="新增" />
                                                                                <a>
                                                                                    <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                                        <path d="M5 2 L5 8"></path>
                                                                                        <path d="M2 5 L8 5"></path>
                                                                                    </svg>
                                                                                </a>
                                                                            </div>
                                                                            <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                                                @foreach ($member as $_member)
                                                                                    <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                                                        <div class="match__item-content">
                                                                                            <span >{{ $_member->Name }}</span>
                                                                                        </div>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="table__body-cust-input">
                                                                        <input type="text" value="{{ $_UserWebsiteContentMember->PercentFrom }}" placeholder="輸入%數" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="table__body-cust-li">
                                                                <div class="table__body-cust-content">
                                                                    <i>
                                                                        <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                                                            <polygon points="2,1 8,1 5,6"></polygon>
                                                                            <polygon points="2,5 8,5 5,9"></polygon>
                                                                        </svg>
                                                                    </i>
                                                                </div>
                                                            </div>
                                                            <div name="second" class="table__body-cust-li">
                                                                <span>姓名 / %數:</span>
                                                                <div class="table__body-cust-content">
                                                                    <div class="table__body-cust-input">
                                                                        <input type="text" placeholder="選擇姓名" member_id="{{ $_UserWebsiteContentMember->MemberTo }}" value="{{ $_UserWebsiteContentMember->MemberToMember->Name }}" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                                                        <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                                        <div class="match">
                                                                            <div class="match_addItem">
                                                                                <input type="text" placeholder="新增" />
                                                                                <a>
                                                                                    <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                                        <path d="M5 2 L5 8"></path>
                                                                                        <path d="M2 5 L8 5"></path>
                                                                                    </svg>
                                                                                </a>
                                                                            </div>
                                                                            <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                                                @foreach ($member as $_member)
                                                                                    <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                                                        <div class="match__item-content">
                                                                                            <span>{{ $_member->Name }}</span>
                                                                                        </div>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="table__body-cust-input">
                                                                        <input type="text" value="{{ $_UserWebsiteContentMember->PercentTo }}" placeholder="輸入%數" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {!! $MainPresenter->getDeleteIcon($website_content_member_key, count($_UserWebsiteContent->UserWebsiteContentMember), $_UserWebsiteContent->Group, $key + 1) !!}
                                                        </div>
                                                        {!! $MainPresenter->getButton($website_content_member_key, count($_UserWebsiteContent->UserWebsiteContentMember), $_UserWebsiteContent->Group, $key + 1, $member) !!}
                                                    </div>
                                                    {!! $MainPresenter->getDefault(count($_UserWebsiteContent->UserWebsiteContentMember), $key + 1, $member, "in") !!}
                                                @endforeach
                                            </div>
                                        @endforeach
                                        <div class="table__body-cust">
                                            <div class="table__body-cust-row">
                                                <a class="table__body-cust_addNewGroup" onclick="newRow('group', {{ $key + 1 }})">
                                                    <i></i>
                                                    <span>增加組別</span>
                                                </a>
                                                <div class="table__body-cust_item">
                                                    <a class="table__body-cust-btn" onclick="setAllReportInformation({{ $key + 1 }})">送出</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div id="" class="table__body-row">
                                <div class="table__body-container">
                                    <div class="table__body-item">
                                        <div class="table__body-content">{{ $total_user_website + 1 }}</div>
                                    </div>
                                    <div class="table__body-item">
                                        <!-- 當輸入的網址有匹配的結果時加上table__body-content_match -->
                                        <div id="website_{{ $total_user_website + 1 }}" class="table__body-content">
                                            <!-- table__body-content-symbol_orange,table__body-content-symbol_blue,table__body-content-symbol_green -->
                                            <div id="website_kind_{{ $total_user_website + 1 }}" class="table__body-content-symbol table__body-content-symbol_orange"></div>
                                            <div id="website_name_{{ $total_user_website + 1 }}" class="table__body-content-title"></div>
                                            <input id="website_url_{{ $total_user_website + 1 }}" website_id="" type="text" value="" onkeyup="setWebsiteDropDownContent(this, {{ $total_user_website + 1 }})" onfocus="setWebsiteDropDown('open', {{ $total_user_website + 1 }})" onblur="setWebsiteDropDown('close', {{ $total_user_website + 1 }})"/>
                                            <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                            <div id="website_drop_down_{{ $total_user_website + 1 }}" class="match">
                                                <ul class="match__container scrollbar-macosx">
                                                    @foreach ($website as $_website)
                                                        <li title="{{ $_website['Name'] }}" class="match__item" onclick="setWebsite(this, {{ $_website['WebsiteId'] }}, {{ $total_user_website + 1 }})">
                                                            <!-- match__item-symbol_orange,match__item-symbol_blue,match__item-symbol_green -->
                                                            <div class="match__item-symbol match__item-symbol_orange">{{ $_website['Kind'] }}</div>
                                                            <div class="match__item-title">{{ $_website['Name'] }}</div>
                                                            <div class="match__item-content">
                                                                <span>{{ $_website['Url'] }}</span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table__body-item">
                                        <div class="table__body-content">
                                            <input id="account_{{ $total_user_website + 1 }}" type="text" placeholder="請輸入帳號" />
                                        </div>
                                    </div>
                                    <div class="table__body-item">
                                        <div class="table__body-content">
                                            <input id="password_{{ $total_user_website + 1 }}" type="text" placeholder="請輸入密碼" />
                                        </div>
                                    </div>
                                    <div class="table__body-item">
                                        <div class="table__body-content">
                                            <input title="{{ $date }}" id="date_start_{{ $total_user_website + 1 }}" class="table__body-content-date" type="date" value="{{ $date }}" />
                                            <div class="table__body-content-range">~</div>
                                            <input title="{{ $date }}" id="date_end_{{ $total_user_website + 1 }}" class="table__body-content-date" type="date" value="{{ $date }}" />
                                            <div class="table__body-content-radio">
                                                <div>
                                                    <input onclick="setDate('last_week', {{ $total_user_website + 1 }})" id="a{{ $total_user_website + 1 }}" type="radio" name="week{{ $total_user_website + 1 }}" />
                                                    <label for="a{{ $total_user_website + 1 }}"></label>上週
                                                </div>
                                                <div>
                                                    <input onclick="setDate('this_week', {{ $total_user_website + 1 }})" id="b{{ $total_user_website + 1 }}" type="radio" name="week{{ $total_user_website + 1 }}" />
                                                    <label for="b{{ $total_user_website + 1 }}"></label>本週
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table__body-item">
                                        <div class="table__body-content">
                                            <a class="table__body-content-run" onclick="getCrawlerColumn({{ $total_user_website + 1 }}, {{ $member }})">抓取</a>
                                            <a class="table__body-content-toggle">
                                                <svg viewBox="0 0 10 10" class="triangle">
                                                    <polygon points="2,1 8,1 5,6"></polygon>
                                                    <polygon points="2,5 8,5 5,9"></polygon>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div id="body_default_{{ $total_user_website + 1 }}" class="table__body-cust-wrap" style="display: none;">
                                    <div name="group_1" class="table__body-cust">
                                        <div class="table__body-cust-row">
                                            <div class="table__body-cust-item">
                                                <!-- 注意只有每一組table__body-cust 的第一個 table__body-cust-row 需要加刪除按鈕和項目編號-->
                                                <a style="width: 20px;">
                                                    <i></i>
                                                </a>
                                                <span>1</span>
                                            </div>
                                            <div class="table__body-cust-item">
                                                <div class="table__body-cust-li">
                                                    <div class="table__body-cust-content">
                                                        <div class="dropDown">
                                                            <div class="dropDown__select">
                                                                <span></span>
                                                                <i class="table__body-content-toggle">
                                                                    <svg viewBox="0 0 10 10">
                                                                        <polygon points="2,1 8,1 5,6"></polygon>
                                                                        <polygon points="2,5 8,5 5,9"></polygon>
                                                                    </svg>
                                                                </i>
                                                            </div>
                                                            <div name="crawler_dropdown_x_{{ $total_user_website + 1 }}" group="x_1" class="dropDown__menu" style="display: none;">
                                                                <div class="dropDown__content scrollbar-macosx">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dropDown">
                                                            <div class="dropDown__select">
                                                                <span></span>
                                                                <i class="table__body-content-toggle">
                                                                    <svg viewBox="0 0 10 10">
                                                                        <polygon points="2,1 8,1 5,6"></polygon>
                                                                        <polygon points="2,5 8,5 5,9"></polygon>
                                                                    </svg>
                                                                </i>
                                                            </div>
                                                            <div name="crawler_dropdown_y_{{ $total_user_website + 1 }}" group="y_1" class="dropDown__menu" style="display: none;">
                                                                <div class="dropDown__content scrollbar-macosx">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a><i class="icn__calculator" onclick="viewPopupCalculate({{ $total_user_website + 1 }}, 1)"></i></a>
                                                        <input name="plus" type="hidden" value="" priority=""/>
                                                        <input name="minus" type="hidden" value="" priority=""/>
                                                        <input name="times" type="hidden" value="" priority=""/>
                                                        <input name="divided" type="hidden" value="" priority=""/>
                                                    </div>
                                                </div>
                                                <div name="first" class="table__body-cust-li">
                                                    <span>姓名 / %數:</span>
                                                    <div class="table__body-cust-content">
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                                            <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                            <div class="match">
                                                                <div class="match_addItem">
                                                                    <input type="text" placeholder="新增" />
                                                                    <a>
                                                                        <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                            <path d="M5 2 L5 8"></path>
                                                                            <path d="M2 5 L8 5"></path>
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                                    @foreach ($member as $_member)
                                                                        <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                                            <div class="match__item-content">
                                                                                <span >{{ $_member->Name }}</span>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="輸入%數" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-li">
                                                    <div class="table__body-cust-content">
                                                        <i>
                                                            <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                                                <polygon points="2,1 8,1 5,6"></polygon>
                                                                <polygon points="2,5 8,5 5,9"></polygon>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                </div>
                                                <div name="second" class="table__body-cust-li">
                                                    <span>姓名 / %數:</span>
                                                    <div class="table__body-cust-content">
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                                            <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                            <div class="match">
                                                                <div class="match_addItem">
                                                                    <input type="text" placeholder="新增" />
                                                                    <a>
                                                                        <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                            <path d="M5 2 L5 8"></path>
                                                                            <path d="M2 5 L8 5"></path>
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                                    @foreach ($member as $_member)
                                                                        <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                                            <div class="match__item-content">
                                                                                <span>{{ $_member->Name }}</span>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="輸入%數" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-del" style="pointer-events: none;">
                                                    {{-- <svg viewBox="0 0 10 10">
                                                        <path d="M1 1 L9 9"></path>
                                                        <path d="M9 1 L1 9"></path>
                                                    </svg> --}}
                                                </div>
                                            </div>
                                            <div class="table__body-cust-item">
                                                <a class="table__body-cust-add" onclick="newRow('name', {{ $total_user_website + 1 }}, 1, {{ $member }})">
                                                    <svg viewBox="0 0 10 10">
                                                        <path d="M2 5 L8 5"></path>
                                                        <path d="M5 2 L5 8"></path>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="table__body-cust-row">
                                            <div class="table__body-cust-item"></div>
                                            <div class="table__body-cust-item">
                                                <div class="table__body-cust-li">
                                                    <div class="table__body-cust-content">
                                                        <!-- <input type="text" /> -->
                                                    </div>
                                                </div>
                                                <div name="first" class="table__body-cust-li">
                                                    <span>姓名 / %數:</span>
                                                    <div class="table__body-cust-content">
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                                            <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                            <div class="match">
                                                                <div class="match_addItem">
                                                                    <input type="text" placeholder="新增" />
                                                                    <a>
                                                                        <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                            <path d="M5 2 L5 8"></path>
                                                                            <path d="M2 5 L8 5"></path>
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                                    @foreach ($member as $_member)
                                                                        <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                                            <div class="match__item-content">
                                                                                <span>{{ $_member->Name }}</span>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="輸入%數" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-li">
                                                    <div class="table__body-cust-content">
                                                        <i>
                                                            <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                                                <polygon points="2,1 8,1 5,6"></polygon>
                                                                <polygon points="2,5 8,5 5,9"></polygon>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                </div>
                                                <div name="second" class="table__body-cust-li">
                                                    <span>姓名 / %數:</span>
                                                    <div class="table__body-cust-content">
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                                            <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                                            <div class="match">
                                                                <div class="match_addItem">
                                                                    <input type="text" placeholder="新增" />
                                                                    <a>
                                                                        <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                                            <path d="M5 2 L5 8"></path>
                                                                            <path d="M2 5 L8 5"></path>
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                                                    @foreach ($member as $_member)
                                                                        <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                                            <div class="match__item-content">
                                                                                <span>{{ $_member->Name }}</span>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="table__body-cust-input">
                                                            <input type="text" placeholder="輸入%數" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="table__body-cust-del" onclick="deleteRow(this, 'name', {{ $total_user_website + 1 }}, 1)" style="pointer-events: none;">
                                                    <svg viewBox="0 0 10 10" style="display: none;">
                                                        <path d="M1 1 L9 9"></path>
                                                        <path d="M9 1 L1 9"></path>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="table__body-cust-item">
                                                <a name="confirm_1" class="table__body-cust-btn" onclick="setReportInformation({{ $total_user_website + 1 }}, 1)">確認</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table__body-cust">
                                        <div class="table__body-cust-row">
                                            <a class="table__body-cust_addNewGroup" onclick="newRow('group', {{ $total_user_website + 1 }})">
                                                <i></i>
                                                <span>增加組別</span>
                                            </a>
                                            <div class="table__body-cust_item">
                                                <a class="table__body-cust-btn" onclick="setAllReportInformation({{ $total_user_website + 1 }})">送出</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <script src="{{asset('/')}}js/ui.js"></script>
        <script type="text/javascript">
            window.onbeforeunload = function() {
                return '';
            };
        </script>
    </body>
</html>
