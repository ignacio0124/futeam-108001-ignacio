<div name="group_{{ $group }}" class="table__body-cust">
    <div class="table__body-cust-row">
        <div class="table__body-cust-item">
            <!-- 注意只有每一組table__body-cust 的第一個 table__body-cust-row 需要加刪除按鈕和項目編號-->
            <a class="table__body-cust-delAll">
                <i onclick="deleteRow(this, 'group', {{ $row }}, {{ $group }})"></i>
            </a>
            <span>{{ $serial_number }}</span>
        </div>
        <div class="table__body-cust-item">
            <div class="table__body-cust-li">
                <div class="table__body-cust-content">
                    <div class="dropDown">
                        <div class="dropDown__select">
                            <span></span>
                            <i class="table__body-content-toggle">
                                <svg viewBox="0 0 10 10">
                                    <polygon points="2,1 8,1 5,6"></polygon>
                                    <polygon points="2,5 8,5 5,9"></polygon>
                                </svg>
                            </i>
                        </div>
                        <div name="crawler_dropdown_x_{{ $row }}" group="x_{{ $group }}" class="dropDown__menu" style="display: none;">
                            <div class="dropDown__content scrollbar-macosx">
                            </div>
                        </div>
                    </div>
                    <div class="dropDown">
                        <div class="dropDown__select">
                            <span></span>
                            <i class="table__body-content-toggle">
                                <svg viewBox="0 0 10 10">
                                    <polygon points="2,1 8,1 5,6"></polygon>
                                    <polygon points="2,5 8,5 5,9"></polygon>
                                </svg>
                            </i>
                        </div>
                        <div name="crawler_dropdown_y_{{ $row }}" group="y_{{ $group }}" class="dropDown__menu" style="display: none;">
                            <div class="dropDown__content scrollbar-macosx">
                            </div>
                        </div>
                    </div>
                    <a><i class="icn__calculator" onclick="viewPopupCalculate({{ $row }}, {{ $group }})"></i></a>
                    <input name="plus" type="hidden" value="" priority=""/>
                    <input name="minus" type="hidden" value="" priority=""/>
                    <input name="times" type="hidden" value="" priority=""/>
                    <input name="divided" type="hidden" value="" priority=""/>
                </div>
            </div>
            <div name="first" class="table__body-cust-li">
                <span>姓名 / %數:</span>
                <div class="table__body-cust-content">
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                        <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                        <div class="match">
                            <div class="match_addItem">
                                <input type="text" placeholder="新增" />
                                <a>
                                    <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                        <path d="M5 2 L5 8"></path>
                                        <path d="M2 5 L8 5"></path>
                                    </svg>
                                </a>
                            </div>
                            <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                @foreach ($member as $_member)
                                    <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                        <div class="match__item-content">
                                            <span >{{ $_member->Name }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="輸入%數" />
                    </div>
                </div>
            </div>
            <div class="table__body-cust-li">
                <div class="table__body-cust-content">
                    <i>
                        <svg viewBox="0 0 10 10" class="triangle triangle_right">
                            <polygon points="2,1 8,1 5,6"></polygon>
                            <polygon points="2,5 8,5 5,9"></polygon>
                        </svg>
                    </i>
                </div>
            </div>
            <div name="second" class="table__body-cust-li">
                <span>姓名 / %數:</span>
                <div class="table__body-cust-content">
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                        <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                        <div class="match">
                            <div class="match_addItem">
                                <input type="text" placeholder="新增" />
                                <a>
                                    <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                        <path d="M5 2 L5 8"></path>
                                        <path d="M2 5 L8 5"></path>
                                    </svg>
                                </a>
                            </div>
                            <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                @foreach ($member as $_member)
                                    <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                        <div class="match__item-content">
                                            <span>{{ $_member->Name }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="輸入%數" />
                    </div>
                </div>
            </div>
            <div class="table__body-cust-del" style="pointer-events: none;">
                {{-- <svg viewBox="0 0 10 10">
                    <path d="M1 1 L9 9"></path>
                    <path d="M9 1 L1 9"></path>
                </svg> --}}
            </div>
        </div>
        <div class="table__body-cust-item">
            <a class="table__body-cust-add" onclick="newRow('name', {{ $row }}, {{ $group }}, {{ $member }})">
                <svg viewBox="0 0 10 10">
                    <path d="M2 5 L8 5"></path>
                    <path d="M5 2 L5 8"></path>
                </svg>
            </a>
        </div>
    </div>
    <div class="table__body-cust-row">
        <div class="table__body-cust-item"></div>
        <div class="table__body-cust-item">
            <div class="table__body-cust-li">
                <div class="table__body-cust-content">
                    <!-- <input type="text" /> -->
                </div>
            </div>
            <div name="first" class="table__body-cust-li">
                <span>姓名 / %數:</span>
                <div class="table__body-cust-content">
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                        <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                        <div class="match">
                            <div class="match_addItem">
                                <input type="text" placeholder="新增" />
                                <a>
                                    <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                        <path d="M5 2 L5 8"></path>
                                        <path d="M2 5 L8 5"></path>
                                    </svg>
                                </a>
                            </div>
                            <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                @foreach ($member as $_member)
                                    <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                        <div class="match__item-content">
                                            <span>{{ $_member->Name }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="輸入%數" />
                    </div>
                </div>
            </div>
            <div class="table__body-cust-li">
                <div class="table__body-cust-content">
                    <i>
                        <svg viewBox="0 0 10 10" class="triangle triangle_right">
                            <polygon points="2,1 8,1 5,6"></polygon>
                            <polygon points="2,5 8,5 5,9"></polygon>
                        </svg>
                    </i>
                </div>
            </div>
            <div name="second" class="table__body-cust-li">
                <span>姓名 / %數:</span>
                <div class="table__body-cust-content">
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="選擇姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                        <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                        <div class="match">
                            <div class="match_addItem">
                                <input type="text" placeholder="新增" />
                                <a>
                                    <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                        <path d="M5 2 L5 8"></path>
                                        <path d="M2 5 L8 5"></path>
                                    </svg>
                                </a>
                            </div>
                            <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                @foreach ($member as $_member)
                                    <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                        <div class="match__item-content">
                                            <span>{{ $_member->Name }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="table__body-cust-input">
                        <input type="text" placeholder="輸入%數" />
                    </div>
                </div>
            </div>
            <div class="table__body-cust-del" onclick="deleteRow(this, 'name', {{ $row }}, {{ $group }})" style="pointer-events: none;">
                <svg viewBox="0 0 10 10" style="display: none;">
                    <path d="M1 1 L9 9"></path>
                    <path d="M9 1 L1 9"></path>
                </svg>
            </div>
        </div>
        <div class="table__body-cust-item">
            <a name="confirm_{{ $group }}" class="table__body-cust-btn" onclick="setReportInformation({{ $row }}, {{ $group }})">確認</a>
        </div>
    </div>
</div>