<div id="" class="table__body-row">
    <div class="table__body-container">
        <div class="table__body-item">
            <div class="table__body-content">{{ $row }}</div>
        </div>
        <div class="table__body-item">
            <!-- 當輸入的網址有匹配的結果時加上table__body-content_match -->
            <div id="website_{{ $row }}" class="table__body-content">
                <!-- table__body-content-symbol_orange,table__body-content-symbol_blue,table__body-content-symbol_green -->
                <div id="website_kind_{{ $row }}" class="table__body-content-symbol table__body-content-symbol_orange"></div>
                <div id="website_name_{{ $row }}" class="table__body-content-title"></div>
                <input id="website_url_{{ $row }}" website_id="" type="text" value="" onkeyup="setWebsiteDropDownContent(this, {{ $row }})" onfocus="setWebsiteDropDown('open', {{ $row }})" onblur="setWebsiteDropDown('close', {{ $row }})"/>
                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                <div id="website_drop_down_{{ $row }}" class="match">
                    <ul class="match__container scrollbar-macosx">
                        @foreach ($website as $_website)
                            <li title="{{ $_website['Name'] }}" class="match__item" onclick="setWebsite(this, {{ $_website['WebsiteId'] }}, {{ $row }})">
                                <!-- match__item-symbol_orange,match__item-symbol_blue,match__item-symbol_green -->
                                <div class="match__item-symbol match__item-symbol_orange">{{ $_website['Kind'] }}</div>
                                <div class="match__item-title">{{ $_website['Name'] }}</div>
                                <div class="match__item-content">
                                    <span>{{ $_website['Url'] }}</span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="table__body-item">
            <div class="table__body-content">
                <input id="account_{{ $row }}" type="text" placeholder="請輸入帳號" />
            </div>
        </div>
        <div class="table__body-item">
            <div class="table__body-content">
                <input id="password_{{ $row }}" type="text" placeholder="請輸入密碼" />
            </div>
        </div>
        <div class="table__body-item">
            <div class="table__body-content">
                <input title="{{ $date }}" id="date_start_{{ $row }}" class="table__body-content-date" type="date" value="{{ $date }}" />
                <div class="table__body-content-range">~</div>
                <input title="{{ $date }}" id="date_end_{{ $row }}" class="table__body-content-date" type="date" value="{{ $date }}" />
                <div class="table__body-content-radio">
                    <div>
                        <input onclick="setDate('last_week', {{ $row }})" id="a{{ $row }}" type="radio" name="week{{ $row }}" />
                        <label for="a{{ $row }}"></label>上週
                    </div>
                    <div>
                        <input onclick="setDate('this_week', {{ $row }})" id="b{{ $row }}" type="radio" name="week{{ $row }}" />
                        <label for="b{{ $row }}"></label>本週
                    </div>
                </div>
            </div>
        </div>
        <div class="table__body-item">
            <div class="table__body-content">
                <a class="table__body-content-run" onclick="getCrawlerColumn({{ $row }}, {{ $member }})">抓取</a>
                <a class="table__body-content-toggle">
                    <svg viewBox="0 0 10 10" class="triangle">
                        <polygon points="2,1 8,1 5,6"></polygon>
                        <polygon points="2,5 8,5 5,9"></polygon>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div id="body_default_{{ $row }}" class="table__body-cust-wrap" style="display: none;">
        <div name="group_1" class="table__body-cust">
            <div class="table__body-cust-row">
                <div class="table__body-cust-item">
                    <!-- 注意只有每一組table__body-cust 的第一個 table__body-cust-row 需要加刪除按鈕和項目編號-->
                    <a style="width: 20px;">
                        <i></i>
                    </a>
                    <span>1</span>
                </div>
                <div class="table__body-cust-item">
                    <div class="table__body-cust-li">
                        <div class="table__body-cust-content">
                            <div class="dropDown">
                                <div class="dropDown__select">
                                    <span></span>
                                    <i class="table__body-content-toggle">
                                        <svg viewBox="0 0 10 10">
                                            <polygon points="2,1 8,1 5,6"></polygon>
                                            <polygon points="2,5 8,5 5,9"></polygon>
                                        </svg>
                                    </i>
                                </div>
                                <div name="crawler_dropdown_x_{{ $row }}" group="x_1" class="dropDown__menu" style="display: none;">
                                    <div class="dropDown__content scrollbar-macosx">
                                    </div>
                                </div>
                            </div>
                            <div class="dropDown">
                                <div class="dropDown__select">
                                    <span></span>
                                    <i class="table__body-content-toggle">
                                        <svg viewBox="0 0 10 10">
                                            <polygon points="2,1 8,1 5,6"></polygon>
                                            <polygon points="2,5 8,5 5,9"></polygon>
                                        </svg>
                                    </i>
                                </div>
                                <div name="crawler_dropdown_y_{{ $row }}" group="y_1" class="dropDown__menu" style="display: none;">
                                    <div class="dropDown__content scrollbar-macosx">
                                    </div>
                                </div>
                            </div>
                            <a><i class="icn__calculator" onclick="viewPopupCalculate({{ $row }}, 1)"></i></a>
                            <input name="plus" type="hidden" value="" priority=""/>
                            <input name="minus" type="hidden" value="" priority=""/>
                            <input name="times" type="hidden" value="" priority=""/>
                            <input name="divided" type="hidden" value="" priority=""/>
                        </div>
                    </div>
                    <div name="first" class="table__body-cust-li">
                        <span>姓名 / %數:</span>
                        <div class="table__body-cust-content">
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                <div class="match">
                                    <div class="match_addItem">
                                        <input type="text" placeholder="新增" />
                                        <a>
                                            <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                <path d="M5 2 L5 8"></path>
                                                <path d="M2 5 L8 5"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                        @foreach ($member as $_member)
                                            <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                <div class="match__item-content">
                                                    <span >{{ $_member->Name }}</span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入%數" />
                            </div>
                        </div>
                    </div>
                    <div class="table__body-cust-li">
                        <div class="table__body-cust-content">
                            <i>
                                <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                    <polygon points="2,1 8,1 5,6"></polygon>
                                    <polygon points="2,5 8,5 5,9"></polygon>
                                </svg>
                            </i>
                        </div>
                    </div>
                    <div name="second" class="table__body-cust-li">
                        <span>姓名 / %數:</span>
                        <div class="table__body-cust-content">
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                <div class="match">
                                    <div class="match_addItem">
                                        <input type="text" placeholder="新增" />
                                        <a>
                                            <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                <path d="M5 2 L5 8"></path>
                                                <path d="M2 5 L8 5"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                        @foreach ($member as $_member)
                                            <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                <div class="match__item-content">
                                                    <span>{{ $_member->Name }}</span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入%數" />
                            </div>
                        </div>
                    </div>
                    <div class="table__body-cust-del" style="pointer-events: none;">
                        {{-- <svg viewBox="0 0 10 10">
                            <path d="M1 1 L9 9"></path>
                            <path d="M9 1 L1 9"></path>
                        </svg> --}}
                    </div>
                </div>
                <div class="table__body-cust-item">
                    <a class="table__body-cust-add" onclick="newRow('name', {{ $row }}, 1, {{ $member }})">
                        <svg viewBox="0 0 10 10">
                            <path d="M2 5 L8 5"></path>
                            <path d="M5 2 L5 8"></path>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="table__body-cust-row">
                <div class="table__body-cust-item"></div>
                <div class="table__body-cust-item">
                    <div class="table__body-cust-li">
                        <div class="table__body-cust-content">
                            <!-- <input type="text" /> -->
                        </div>
                    </div>
                    <div name="first" class="table__body-cust-li">
                        <span>姓名 / %數:</span>
                        <div class="table__body-cust-content">
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)" />
                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                <div class="match">
                                    <div class="match_addItem">
                                        <input type="text" placeholder="新增" />
                                        <a>
                                            <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                <path d="M5 2 L5 8"></path>
                                                <path d="M2 5 L8 5"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                        @foreach ($member as $_member)
                                            <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                <div class="match__item-content">
                                                    <span>{{ $_member->Name }}</span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入%數" />
                            </div>
                        </div>
                    </div>
                    <div class="table__body-cust-li">
                        <div class="table__body-cust-content">
                            <i>
                                <svg viewBox="0 0 10 10" class="triangle triangle_right">
                                    <polygon points="2,1 8,1 5,6"></polygon>
                                    <polygon points="2,5 8,5 5,9"></polygon>
                                </svg>
                            </i>
                        </div>
                    </div>
                    <div name="second" class="table__body-cust-li">
                        <span>姓名 / %數:</span>
                        <div class="table__body-cust-content">
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入姓名" member_id="" onkeyup="setNameDropDownContent(this)" onfocus="setNameDropDown(this)"/>
                                <!-- 當輸入的網址有匹配的結果時加上match_visible -->
                                <div class="match">
                                    <div class="match_addItem">
                                        <input type="text" placeholder="新增" />
                                        <a>
                                            <svg viewBox="0 0 10 10" onclick="newMember(this)">
                                                <path d="M5 2 L5 8"></path>
                                                <path d="M2 5 L8 5"></path>
                                            </svg>
                                        </a>
                                    </div>
                                    <ul name="name_drop_down" class="match__container scrollbar-macosx">
                                        @foreach ($member as $_member)
                                            <li class="match__item" onclick="setName(this, '{{ $_member->Name }}', {{ $_member->MemberId }})">
                                                <div class="match__item-content">
                                                    <span>{{ $_member->Name }}</span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="table__body-cust-input">
                                <input type="text" placeholder="輸入%數" />
                            </div>
                        </div>
                    </div>
                    <div class="table__body-cust-del" onclick="deleteRow(this, 'name', {{ $row }}, 1)" style="pointer-events: none;">
                        <svg viewBox="0 0 10 10" style="display: none;">
                            <path d="M1 1 L9 9"></path>
                            <path d="M9 1 L1 9"></path>
                        </svg>
                    </div>
                </div>
                <div class="table__body-cust-item">
                    <a name="confirm_1" class="table__body-cust-btn" onclick="setReportInformation({{ $row }}, 1)">確認</a>
                </div>
            </div>
        </div>
        <div class="table__body-cust">
            <div class="table__body-cust-row">
                <a class="table__body-cust_addNewGroup" onclick="newRow('group', {{ $row }})">
                    <i></i>
                    <span>增加組別</span>
                </a>
                <div class="table__body-cust_item">
                    <a class="table__body-cust-btn" onclick="setAllReportInformation({{ $row }})">送出</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('/')}}js/ui.js"></script>