<!DOCTYPE html>
<html lang="en">
    <head>
        <title>計算</title>
        <meta charset="UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="{{asset('/')}}css/reset.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/jquery.scrollbar.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/popup.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/tool.module.css" />
        <script src="{{asset('/')}}js/jquery.js"></script>
        <script src="{{asset('/')}}js/jquery.scrollbar.min.js"></script>
        <script src="{{asset('/')}}js/Popup.js"></script>
    </head>
    <body>
        @inject('PopupCalculatePresenter','App\Presenter\PopupCalculatePresenter')
        <div class="popupWrap">
            <div class="popup">
                <div class="popup__title">
                    <a id="plus_btn" class="popup__functionBtn" onclick="setCalculateRow('plus', '{{ json_encode($crawler_column) }}', {{ $row }}, {{ $group }})">＋</a>
                    <a id="minus_btn" class="popup__functionBtn" onclick="setCalculateRow('minus', '{{ json_encode($crawler_column) }}', {{ $row }}, {{ $group }})">－</a>
                    <a id="times_btn" class="popup__functionBtn" onclick="setCalculateRow('times', '{{ json_encode($crawler_column) }}', {{ $row }}, {{ $group }})">×</a>
                    <a id="divided_btn" class="popup__functionBtn" onclick="setCalculateRow('divided', '{{ json_encode($crawler_column) }}', {{ $row }}, {{ $group }})">÷</a>
                </div>
                <div class="popup__content">
                    <div id="calculator_row" class="popup__calculator scrollbar-macosx">
                        {!! $PopupCalculatePresenter->getCalculateRow($calculate, $crawler_column, $row, $group) !!}
                    </div>
                </div>
                <div class="popup__bottom">
                    <a class="popup__btn popup__btn_cancel" onclick="closePopupCalculate()">取消</a>
                    <a class="popup__btn popup__btn_confrim" onclick="confirmPopupCalculate({{ $row }}, {{ $group }})">確定</a>
                </div>
            </div>
        </div>
        <script>
            $(".scrollbar-macosx").scrollbar();
            /** @type {number} 優先順序 */
            var plus_priority = $("#body_default_" + {{ $row }} + " div[name='group_" + {{ $group }} + "'] input[name='plus']").attr("priority");
            /** @type {number} 優先順序 */
            var minus_priority = $("#body_default_" + {{ $row }} + " div[name='group_" + {{ $group }} + "'] input[name='minus']").attr("priority");
            /** @type {number} 優先順序 */
            var times_priority = $("#body_default_" + {{ $row }} + " div[name='group_" + {{ $group }} + "'] input[name='times']").attr("priority");
            /** @type {number} 優先順序 */
            var divided_priority = $("#body_default_" + {{ $row }} + " div[name='group_" + {{ $group }} + "'] input[name='divided']").attr("priority");

            if (plus_priority != "") {
                $("#plus_btn").css("pointer-events", "none");
            }
            if (minus_priority != "") {
                $("#minus_btn").css("pointer-events", "none");
            }
            if (times_priority != "") {
                $("#times_btn").css("pointer-events", "none");
            }
            if (divided_priority != "") {
                $("#divided_btn").css("pointer-events", "none");
            }
        </script>
    </body>
</html>
