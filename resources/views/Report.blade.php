<!DOCTYPE html>
<html lang="en">
    <head>
        <title>報表</title>
        <meta charset="UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link rel="stylesheet" href="{{asset('/')}}css/reset.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/jquery.scrollbar.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/icon.css" />
        <link rel="stylesheet" href="{{asset('/')}}css/showRecord.css" />
        <script src="{{asset('/')}}js/jquery.js"></script>
        <script src="{{asset('/')}}js/jquery.scrollbar.min.js"></script>
        <script src="{{asset('/')}}js/Report.js"></script>
        <script src="{{asset('/')}}js/Common.js"></script>
    </head>
    <body>
        @inject('ReportPresenter','App\Presenter\ReportPresenter')
        <div class="wrap">
            <header class="header">
                <div class="header__states">
                    <div class="header__states-icon icn__home"></div>
                    <div class="dropDown">
                        <div class="dropDown__select">
                            <span>報表</span>
                            <i>
                                <svg viewBox="0 0 10 10" class="triangle">
                                    <polygon points="2,3 8,3 5,8"></polygon>
                                </svg>
                            </i>
                        </div>
                        <div class="dropDown__menu" style="display: none;">
                            <div class="dropDown__content scrollbar-macosx">
                                <div class="dropDown__li" onclick="changePage('Report', 'Main')">主頁</div>
                                <div class="dropDown__li" onclick="changePage('Report','Backstage')">後台</div>
                                <div class="dropDown__li">報表</div>
                                <div class="dropDown__li" onclick="window.location='{{ URL('Logout') }}'">登出</div>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="header__user">{{ $account }}</span>
            </header>
            <main class="main">
                <div class="main__header">
                    <span class="main__header-title">REPORT</span>
                    <!-- <a class="main__header-btn">增加欄位</a> -->
                    <div class="main__filter">
                        <div class="main__filter-picker main__filter-picker_time">
                            <div class="main__filter-picker-icon icn__time"></div>
                            <div class="dropDown">
                                <div class="dropDown__select">
                                    <span>{{ $date }}</span>
                                    <i>
                                        <svg viewBox="0 0 10 10" class="triangle">
                                            <polygon points="2,1 8,1 5,6"></polygon>
                                            <polygon points="2,5 8,5 5,9"></polygon>
                                        </svg>
                                    </i>
                                </div>
                                <div class="dropDown__menu" style="display: none;">
                                    <div class="dropDown__content scrollbar-macosx">
                                        @foreach ($report_date_group as $_report_date_group)
                                            <div onclick="viewDateReport('{{ $_report_date_group }}')" class="dropDown__li">{{ $_report_date_group }}</div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main__filter-picker main__filter-picker_member">
                            <div class="main__filter-picker-icon icn__person"></div>
                            <div class="dropDown">
                                <div class="dropDown__select">
                                    <span>人名篩選</span>
                                    <i>
                                        <svg viewBox="0 0 10 10" class="triangle">
                                            <polygon points="2,1 8,1 5,6"></polygon>
                                            <polygon points="2,5 8,5 5,9"></polygon>
                                        </svg>
                                    </i>
                                </div>
                                <div class="dropDown__menu" style="display: none;">
                                    <label class="dropDown__checkbox dropDown__checkbox_default" for="all">
                                        <input onclick="setAllMemberFilter(this)" type="checkbox" id="all" checked/>
                                        <div class="checkbox"></div>
                                        <span>全選</span>
                                    </label>
                                    <div class="dropDown__content scrollbar-macosx">
                                        @foreach ($member as $_member)
                                            <label class="dropDown__checkbox" for="{{ $_member["MemberId"] }}">
                                                <input onclick="setMemberFilter({{ $_member["Number"] }}, this)" type="checkbox" id="{{ $_member["MemberId"] }}" checked/>
                                                <div class="checkbox"></div>
                                                <span>{{ $_member["Name"] }}</span>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main__body">
                    <div class="board">
                        <div class="board__left">
                            <div class="board__header">
                                <div class="board__header-item">#</div>
                                <div class="board__header-item">版</div>
                                <div class="board__header-item">帳號</div>
                            </div>
                            <div class="board__body">
                                <div id="website" class="slideBar">
                                    @foreach ($user_website as $_user_website)
                                        {!! $ReportPresenter->getReportWebsite($_user_website, $date) !!}
                                    @endforeach
                                    <!-- 在最後留一個空的    board__row -->
                                    <div class="board__row"></div>
                                </div>
                            </div>
                            <div class="board__footer">
                                <div class="board__footer-item">Total</div>
                            </div>
                        </div>

                        <div class="board__right">
                            <div class="board__header">
                                <div class="slideBar">
                                    @foreach ($member as $_member)
                                        <div name="{{ $_member["Number"] }}" class="board__header-item"><div>{{ $_member->Name }}</div></div>
                                    @endforeach
                                </div>
                            </div>
                            <div id="money" class="board__body scrollbar-macosx">
                                @foreach ($user_website as $_user_website)
                                    {!! $ReportPresenter->getMemberMoney($_user_website, $report_website, $member, $date) !!}
                                @endforeach
                                <div class="board__row"></div>
                            </div>
                            <div class="board__footer">
                                <div id="total" class="slideBar">
                                    @foreach ($member as $_member)
                                        <div name="{{ $_member["Number"] }}" class="board__footer-item board__footer-item_positive"></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <script src="{{asset('/')}}js/ui.js"></script>
        <script>
            $(document).ready(function() {
                /** 設定Total */
                $("#total div").each(function() {
                    var website_id = $(this).attr("name");
                    var total_money = 0;
                    $("#money .board__row").each(function() {
                        if ($(this).find("div[name='" + website_id + "']").text() != "") {
                            total_money += parseInt($(this).find("div[name='" + website_id + "']").text());
                        }
                    });
                    if (total_money < 0) {
                        $(this).attr("class", "board__footer-item board__footer-item_negative");
                    }
                    $(this).text(total_money);
                });

                /** 設定流水號 */
                $("#website div[class='board__row']").each(function(key) {
                    $(this).find("div").eq(0).text(key + 1);
                });
            });
        </script>
    </body>
</html>
