<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** 登入頁 */
Route::get('/', function () {
    return view('Login');
});
/** 主頁 */
Route::get('Main', 'MainController@viewMainPage');
/** 報表 */
Route::get('Report/{date?}', 'ReportController@viewReportPage');
/** 後台-成員管理 */
Route::get('Backstage/Member', 'BackstageController@viewBackstageMemberPage');
/** 登出 */
Route::get('Logout', 'Auth\LoginController@Logout');
/** 計算彈跳視窗 */
Route::get('Popup/Calculate', 'MainController@viewPopupCalculate');
/** 登入驗證 */
Route::post('Login', 'Auth\LoginController@Login');
/** 取得爬蟲抓取的欄位值 */
Route::post('getCrawlerColumn', 'MainController@getCrawlerColumn');
/** 取得增加欄位的div */
Route::post('getRowDiv', 'MainController@getRowDiv');
/** 取得增加組別的div */
Route::post('getGroupDiv', 'MainController@getGroupDiv');
/** 新增成員 */
Route::post('newMember', 'BackstageController@newMember');
/** 刪除成員 */
Route::post('deleteMember', 'BackstageController@deleteMember');
/** 刪除儲存的網站內容 */
Route::post('deleteWebsiteContent', 'MainController@deleteWebsiteContent');
/** 編輯成員 */
Route::post('setMember', 'BackstageController@setMember');
/** 設定使用者的網址 */
Route::post('setUserWebsite', 'MainController@setUserWebsite');
/** 設定儲存內容 */
Route::post('setSaveContent', 'MainController@setSaveContent');
/** 設定報表 */
Route::post('setReport', 'ReportController@setReport');


// Route::get('password', function () {
// 	return password_hash('123', PASSWORD_BCRYPT);
// });
